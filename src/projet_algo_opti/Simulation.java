/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_algo_opti;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import static java.awt.Font.BOLD;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.layout.Border;
import static javafx.scene.text.Font.font;
import javax.swing.DefaultListModel;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author GILLES KEMGOUM
 */
public class Simulation extends javax.swing.JFrame {

    /**
     * Creates new form Projet_Algo
     */
    
    public Simulation() {
        
        initComponents();
        prog1.setValue(0);
        //Récupération des conteneurs de la plateforme initiale
        conteneurs[0][0]=j00;
        conteneurs[0][1]=j01;
        conteneurs[0][2]=j02;
        conteneurs[0][3]=j03;
        conteneurs[0][4]=j04;
        conteneurs[0][5]=j05;
        conteneurs[0][6]=j06;
        conteneurs[0][7]=j07;
        
        conteneurs[1][0]=j10;
        conteneurs[1][1]=j11;
        conteneurs[1][2]=j12;
        conteneurs[1][3]=j13;
        conteneurs[1][4]=j14;
        conteneurs[1][5]=j15;
        conteneurs[1][6]=j16;
        conteneurs[1][7]=j17;
         
        conteneurs[2][0]=j20;
        conteneurs[2][1]=j21;
        conteneurs[2][2]=j22;
        conteneurs[2][3]=j23;
        conteneurs[2][4]=j24;
        conteneurs[2][5]=j25;
        conteneurs[2][6]=j26;
        conteneurs[2][7]=j27;
        
        conteneurs[3][0]=j30;
        conteneurs[3][1]=j31;
        conteneurs[3][2]=j32;
        conteneurs[3][3]=j33;
        conteneurs[3][4]=j34;
        conteneurs[3][5]=j35;
        conteneurs[3][6]=j36;
        conteneurs[3][7]=j37;
        
        
        finales[0][0]=finale00;
        finales[0][1]=finale01;
        finales[0][2]=finale02;
        finales[0][3]=finale03;
        finales[0][4]=finale04;
        finales[0][5]=finale05;
        finales[0][6]=finale06;
        finales[0][7]=finale07;
        
        finales[1][0]=finale10;
        finales[1][1]=finale11;
        finales[1][2]=finale12;
        finales[1][3]=finale13;
        finales[1][4]=finale14;
        finales[1][5]=finale15;
        finales[1][6]=finale16;
        finales[1][7]=finale17;
       
         
        finales[2][0]=finale20;
        finales[2][1]=finale21;
        finales[2][2]=finale22;
        finales[2][3]=finale23;
        finales[2][4]=finale24;
        finales[2][5]=finale25;
        finales[2][6]=finale26;
        finales[2][7]=finale27; 
        
        
        initiales[0][0]=initiale00;
        initiales[0][1]=initiale01;
        initiales[0][2]=initiale02;
        initiales[0][3]=initiale03;
        initiales[0][4]=initiale04;
        initiales[0][5]=initiale05;
        initiales[0][6]=initiale06;
        initiales[0][7]=initiale07;
        
        initiales[1][0]=initiale10;
        initiales[1][1]=initiale11;
        initiales[1][2]=initiale12;
        initiales[1][3]=initiale13;
        initiales[1][4]=initiale14;
        initiales[1][5]=initiale15;
        initiales[1][6]=initiale16;
        initiales[1][7]=initiale17;
         
        initiales[2][0]=initiale20;
        initiales[2][1]=initiale21;
        initiales[2][2]=initiale22;
        initiales[2][3]=initiale23;
        initiales[2][4]=initiale24;
        initiales[2][5]=initiale25;
        initiales[2][6]=initiale26;
        initiales[2][7]=initiale27;
         
        j27.getParent().add(bras);
       
        bras.setEditable(false);
        bras.setBackground(Color.ORANGE);
        bras.setBorder(new EmptyBorder(2,2,2,2));
        bras.setBounds(brasVisible.getX(),brasVisible.getY(),brasVisible.getWidth(),brasVisible.getHeight());
        brasVisible.getParent().remove(brasVisible);        
        
        //Initialisation des composants
         
        System.out.println("Positions initiales des conteneurs");
         for(int i=0; i<4; i++){
            for(int j=0; j<8; j++){
            System.out.print("("+i+":"+j+")"+conteneurs[i][j].getText()+" "+conteneurs[i][j].getX()+","+conteneurs[i][j].getY()+"   ");
            } 
            System.out.println();
            
         } 
         
         initiales[0][1].setText("2");
         initiales[0][2].setText("1");
         initiales[0][3].setText("3");
         initiales[0][4].setText("7");
         initiales[0][5].setText("8");
         initiales[0][6].setText("15");
         initiales[1][1].setText("9");
         initiales[2][1].setText("10");
         initiales[1][5].setText("6");
         initiales[2][5].setText("5");
         initiales[1][6].setText("16");
         initiales[2][6].setText("4");
         
         
         finales[0][1].setText("1");
         finales[0][2].setText("2");
         finales[0][3].setText("3");
         finales[0][4].setText("4");
         finales[0][5].setText("5");
         finales[0][6].setText("6");
         finales[1][1].setText("7");
         finales[1][2].setText("8");
         finales[1][3].setText("9");
         finales[1][4].setText("10");
         finales[1][5].setText("15");
         finales[1][6].setText("16");
         
    }
    private void arranger(){
        for(int i=0; i<8; i++){
           initiales[3][i] = new JFormattedTextField();  
           finales[3][i] = new JFormattedTextField();  
        }           
        for(int i=0; i<4; i++){
            for(int j=0; j<8; j++){
                conteneurs[i][j].setText(initiales[i][j].getText());
                   conteneurs[i][j].setFont(new Font(Font.SERIF, Font.ITALIC, 25)); 
                   conteneurs[i][j].setEnabled(true);
                   conteneurs[i][j].setVisible(true);
                   conteneurs[i][j].setBackground(Color.GREEN);
                   conteneurs[i][j].setEditable(false);
                if(initiales[i][j].getText().equals("")){
                    conteneurs[i][j].setVisible(false);
                }
            }
        }
        
    }

private void checkRepetition(JFormattedTextField jt){
        for(int i=0; i<3; i++){
            for(int j=0; j<8; j++){
                if((initiales[i][j]!=jt) && (initiales[i][j].getText().equals(jt.getText())) && (jt.getText().length()!=0)){
                
                    if(i==0 && initiales[1][j].getText().length()!=0){
                        initiales[i][j].setText(initiales[1][j].getText());
                        if(initiales[2][j].getText().length()!=0){
                           initiales[1][j].setText(initiales[2][j].getText());
                           initiales[2][j].setText("");
                        }
                        else{
                           initiales[1][j].setText("");
                           initiales[2][j].setEditable(false);
                        }
                    }
                    else if(i==0 && initiales[1][j].getText().length()==0){
                        initiales[i][j].setText("");
                        initiales[1][j].setEditable(false);
                    }
                    else if(i==1 && initiales[2][j].getText().length()!=0){
                        initiales[1][j].setText(initiales[2][j].getText());
                        initiales[2][j].setText("");
                    }
                    else if(i==1 && initiales[2][j].getText().length()==0){
                        initiales[1][j].setText("");
                        initiales[2][j].setEditable(false);
                    }
                    //au cas où on est au sommet c-a-d à la ligne 3/3
                    else initiales[i][j].setText("");
                }
            }
        }
    }
private void checkRepetitionF(JFormattedTextField jt){
    if(!checkExistance(jt)){
           JOptionPane.showMessageDialog(null,"Conteneur '"+jt.getText()+"' introuvable","Erreur de correspondance",0);
           jt.setText("");
        for(int i=0; i<3; i++){
               for(int j=0; j<8; j++){
                  if(finales[i][j]==jt){
                     if(i==0){
                            if(!finales[1][j].getText().equals("")){
                                finales[i][j].setText(finales[1][j].getText());
                                finales[1][j].setText("");
                                if(!finales[2][j].getText().equals("")){
                                    finales[1][j].setText(finales[2][j].getText());
                                    finales[2][j].setText("");
                                }
                                else finales[2][j].setEditable(false);
                            }
                            else {
                                finales[1][j].setEditable(false);
                                jt.setText("");
                            }
                        }
                       else if(i==1){
                            if(!finales[2][j].getText().equals("")){
                                finales[i][j].setText(finales[2][j].getText());
                            }
                            else {
                                finales[1][j].setText("");
                                finales[2][j].setEditable(false);
                            } 
                        }
                       else finales[2][j].setText("");
                 
                    } 
               }
            }
        } 
    realCheckRepetitionF(jt) ;
    statut();
}
private static void statut(){
        for(int i=0; i<3; i++){
            for(int j=0; j<8; j++){
                initiales[i][j].setBackground(Color.CYAN);
                if(!(initiales[i][j].getText().equals(""))){
                    for(int P=0; P<3; P++){
                        for(int q=0; q<8; q++){
                           if(finales[P][q].getText().equals(initiales[i][j].getText())){
                              initiales[i][j].setBackground(Color.green);
                           }
                        } 
                    }  
                }    
            } 
        }
    }
private void realCheckRepetitionF(JFormattedTextField jt){ 
        
        for(int i=0; i<3; i++){
            for(int j=0; j<8; j++){
                if((finales[i][j]!=jt) && (finales[i][j].getText().equals(jt.getText())) && (jt.getText().length()!=0)){
                
                    if(i==0 && finales[1][j].getText().length()!=0){
                        finales[i][j].setText(finales[1][j].getText());
                        if(finales[2][j].getText().length()!=0){
                           finales[1][j].setText(finales[2][j].getText());
                           finales[2][j].setText("");
                        }
                        else{
                           finales[1][j].setText("");
                           finales[2][j].setEditable(false);
                        }
                    }
                    else if(i==0 && finales[1][j].getText().length()==0){
                        finales[i][j].setText("");
                        finales[1][j].setEditable(false);
                    }
                    else if(i==1 && finales[2][j].getText().length()!=0){
                        finales[1][j].setText(finales[2][j].getText());
                        initiales[2][j].setText("");
                    }
                    else if(i==1 && finales[2][j].getText().length()==0){
                        finales[1][j].setText("");
                        finales[2][j].setEditable(false);
                    }
                    //au cas où on est au sommet c-a-d à la ligne 3/3
                    else finales[i][j].setText("");
                }
            }
        }
    }
private boolean checkExistance(JFormattedTextField jtf){
         for(int i=0; i<3; i++){
            for(int j=0; j<8; j++){    
               if(jtf.getText().equals(initiales[i][j].getText())){  
                   return true;
               }
            }
        } 
         
        return false;
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Valeur = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        l20 = new javax.swing.JLabel();
        l26 = new javax.swing.JLabel();
        l27 = new javax.swing.JLabel();
        l17 = new javax.swing.JLabel();
        l07 = new javax.swing.JLabel();
        l16 = new javax.swing.JLabel();
        l06 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        l10 = new javax.swing.JLabel();
        l00 = new javax.swing.JLabel();
        l21 = new javax.swing.JLabel();
        l11 = new javax.swing.JLabel();
        l01 = new javax.swing.JLabel();
        l22 = new javax.swing.JLabel();
        l12 = new javax.swing.JLabel();
        l02 = new javax.swing.JLabel();
        l23 = new javax.swing.JLabel();
        l13 = new javax.swing.JLabel();
        l03 = new javax.swing.JLabel();
        l24 = new javax.swing.JLabel();
        l14 = new javax.swing.JLabel();
        l04 = new javax.swing.JLabel();
        l25 = new javax.swing.JLabel();
        l15 = new javax.swing.JLabel();
        l05 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jPanel4 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jToolBar1 = new javax.swing.JToolBar();
        jLabel23 = new javax.swing.JLabel();
        nbTransition = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        ec = new javax.swing.JLabel();
        jScrollBar1 = new javax.swing.JScrollBar();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();
        pan1 = new javax.swing.JPanel();
        pan2 = new javax.swing.JPanel();
        pan3 = new javax.swing.JPanel();
        pan4 = new javax.swing.JPanel();
        j10 = new javax.swing.JFormattedTextField();
        j11 = new javax.swing.JFormattedTextField();
        j12 = new javax.swing.JFormattedTextField();
        j13 = new javax.swing.JFormattedTextField();
        j03 = new javax.swing.JFormattedTextField();
        j02 = new javax.swing.JFormattedTextField();
        j01 = new javax.swing.JFormattedTextField();
        j00 = new javax.swing.JFormattedTextField();
        j20 = new javax.swing.JFormattedTextField();
        j21 = new javax.swing.JFormattedTextField();
        j22 = new javax.swing.JFormattedTextField();
        j23 = new javax.swing.JFormattedTextField();
        j04 = new javax.swing.JFormattedTextField();
        j14 = new javax.swing.JFormattedTextField();
        j24 = new javax.swing.JFormattedTextField();
        j05 = new javax.swing.JFormattedTextField();
        j15 = new javax.swing.JFormattedTextField();
        j25 = new javax.swing.JFormattedTextField();
        j06 = new javax.swing.JFormattedTextField();
        j16 = new javax.swing.JFormattedTextField();
        j26 = new javax.swing.JFormattedTextField();
        j07 = new javax.swing.JFormattedTextField();
        j17 = new javax.swing.JFormattedTextField();
        j27 = new javax.swing.JFormattedTextField();
        support = new java.awt.Button();
        brasVisible = new javax.swing.JFormattedTextField();
        fil = new java.awt.Button();
        j30 = new javax.swing.JFormattedTextField();
        j31 = new javax.swing.JFormattedTextField();
        j32 = new javax.swing.JFormattedTextField();
        j33 = new javax.swing.JFormattedTextField();
        j34 = new javax.swing.JFormattedTextField();
        j35 = new javax.swing.JFormattedTextField();
        j36 = new javax.swing.JFormattedTextField();
        j37 = new javax.swing.JFormattedTextField();
        jLabel11 = new javax.swing.JLabel();
        simuler = new java.awt.Button();
        jPanel6 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        initiale00 = new javax.swing.JFormattedTextField();
        initiale10 = new javax.swing.JFormattedTextField();
        initiale20 = new javax.swing.JFormattedTextField();
        initiale21 = new javax.swing.JFormattedTextField();
        initiale22 = new javax.swing.JFormattedTextField();
        initiale23 = new javax.swing.JFormattedTextField();
        initiale24 = new javax.swing.JFormattedTextField();
        initiale25 = new javax.swing.JFormattedTextField();
        initiale26 = new javax.swing.JFormattedTextField();
        initiale11 = new javax.swing.JFormattedTextField();
        initiale01 = new javax.swing.JFormattedTextField();
        initiale02 = new javax.swing.JFormattedTextField();
        initiale12 = new javax.swing.JFormattedTextField();
        initiale03 = new javax.swing.JFormattedTextField();
        initiale04 = new javax.swing.JFormattedTextField();
        initiale05 = new javax.swing.JFormattedTextField();
        initiale06 = new javax.swing.JFormattedTextField();
        initiale07 = new javax.swing.JFormattedTextField();
        initiale13 = new javax.swing.JFormattedTextField();
        initiale14 = new javax.swing.JFormattedTextField();
        initiale15 = new javax.swing.JFormattedTextField();
        initiale17 = new javax.swing.JFormattedTextField();
        initiale16 = new javax.swing.JFormattedTextField();
        initiale27 = new javax.swing.JFormattedTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        finale00 = new javax.swing.JFormattedTextField();
        finale10 = new javax.swing.JFormattedTextField();
        finale20 = new javax.swing.JFormattedTextField();
        finale21 = new javax.swing.JFormattedTextField();
        finale22 = new javax.swing.JFormattedTextField();
        finale23 = new javax.swing.JFormattedTextField();
        finale24 = new javax.swing.JFormattedTextField();
        finale25 = new javax.swing.JFormattedTextField();
        finale26 = new javax.swing.JFormattedTextField();
        finale11 = new javax.swing.JFormattedTextField();
        finale01 = new javax.swing.JFormattedTextField();
        finale02 = new javax.swing.JFormattedTextField();
        finale12 = new javax.swing.JFormattedTextField();
        finale03 = new javax.swing.JFormattedTextField();
        finale04 = new javax.swing.JFormattedTextField();
        finale05 = new javax.swing.JFormattedTextField();
        finale06 = new javax.swing.JFormattedTextField();
        finale07 = new javax.swing.JFormattedTextField();
        finale13 = new javax.swing.JFormattedTextField();
        finale14 = new javax.swing.JFormattedTextField();
        finale15 = new javax.swing.JFormattedTextField();
        finale17 = new javax.swing.JFormattedTextField();
        finale16 = new javax.swing.JFormattedTextField();
        finale27 = new javax.swing.JFormattedTextField();
        jPanel7 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        prog2 = new javax.swing.JProgressBar();
        prog1 = new javax.swing.JProgressBar();
        v = new javax.swing.JSlider();
        jLabel16 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        ListeTransitions = new javax.swing.JList<>();
        jLabel22 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jLabel28 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        latt = new javax.swing.JComboBox<>();
        jLabel17 = new javax.swing.JLabel();
        Load = new javax.swing.JToggleButton();
        reload = new javax.swing.JToggleButton();
        jPanel12 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();

        Valeur.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        Valeur.setForeground(new java.awt.Color(0, 102, 102));
        Valeur.setText("Ok");

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 102, 102));
        jLabel2.setText("Conteneurs à ranger:");

        l20.setText("jLabel4");

        l26.setText("jLabel22");

        l27.setText("jLabel23");

        l17.setText("jLabel26");

        l07.setText("jLabel27");

        l16.setText("jLabel24");

        l06.setText("jLabel25");

        jPanel2.setBackground(new java.awt.Color(204, 204, 255));

        l10.setText("jLabel5");

        l00.setText("jLabel6");

        l21.setText("jLabel7");

        l11.setText("jLabel8");

        l01.setText("jLabel9");

        l22.setText("jLabel10");

        l12.setText("jLabel11");

        l02.setText("jLabel12");

        l23.setText("jLabel13");

        l13.setText("jLabel14");

        l03.setText("jLabel15");

        l24.setText("jLabel16");

        l14.setText("jLabel17");

        l04.setText("jLabel18");

        l25.setText("jLabel19");

        l15.setText("jLabel20");

        l05.setText("jLabel21");

        jLabel4.setText("----------------------------------------------------------------------------------------------------------------------------------------------");

        jLabel5.setText("----------------------------------------------------------------------------------------------------------------------------------------------");

        jLabel6.setText("|");

        jLabel7.setText("|");

        jLabel8.setText("|");

        jLabel9.setText("|");

        jLabel10.setText("|");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(155, 155, 155)
                        .addComponent(jLabel6))
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(l10)
                            .addComponent(l00))
                        .addGap(32, 32, 32)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(l21)
                                .addGap(31, 31, 31)
                                .addComponent(l22)
                                .addGap(32, 32, 32)
                                .addComponent(l23))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(l11)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                                        .addComponent(l12))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(l01)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(l02)))
                                .addGap(32, 32, 32)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(l03)
                                    .addComponent(l13))))
                        .addGap(46, 46, 46)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(l24)
                                .addGap(33, 33, 33)
                                .addComponent(l25))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(l04)
                                    .addComponent(l14))
                                .addGap(33, 33, 33)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(l15)
                                    .addComponent(l05))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel8)
                            .addComponent(jLabel10))
                        .addContainerGap())
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(l21)
                    .addComponent(l22)
                    .addComponent(l23)
                    .addComponent(l24)
                    .addComponent(l25)
                    .addComponent(jLabel8))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(l05)
                            .addComponent(l04))
                        .addContainerGap())
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(l10)
                            .addComponent(l11)
                            .addComponent(l12)
                            .addComponent(l13)
                            .addComponent(l14)
                            .addComponent(l15))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(l00)
                                    .addComponent(l01)
                                    .addComponent(l02)
                                    .addComponent(l03)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel7)))))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 186, Short.MAX_VALUE)
        );

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        jPanel4.setBackground(new java.awt.Color(255, 204, 255));

        jLabel13.setFont(new java.awt.Font("Bookman Old Style", 2, 18)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(51, 51, 51));
        jLabel13.setText("Configuration finale");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel13)
                .addGap(98, 98, 98))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTextField3.setText("jTextField3");

        jToolBar1.setRollover(true);

        jLabel23.setText("jLabel23");

        nbTransition.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        nbTransition.setForeground(new java.awt.Color(255, 51, 0));
        nbTransition.setText("0");

        jLabel27.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(255, 153, 153));
        jLabel27.setText("0");

        ec.setBackground(new java.awt.Color(0, 255, 255));
        ec.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        ec.setForeground(new java.awt.Color(0, 153, 153));
        ec.setText("0");

        jScrollPane1.setViewportView(jTextPane1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocation(new java.awt.Point(30, 0));
        setResizable(false);

        pan1.setBackground(new java.awt.Color(255, 255, 204));

        pan2.setBackground(new java.awt.Color(204, 255, 204));

        pan3.setBackground(new java.awt.Color(255, 204, 204));
        pan3.setAutoscrolls(true);
        pan3.setMinimumSize(new java.awt.Dimension(32767, 32767));

        pan4.setBackground(new java.awt.Color(156, 227, 237));
        pan4.setMinimumSize(new java.awt.Dimension(32767, 32767));

        j10.setEnabled(false);
        j10.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j10FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j10FocusLost(evt);
            }
        });

        j11.setEnabled(false);
        j11.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j11FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j11FocusLost(evt);
            }
        });
        j11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                j11ActionPerformed(evt);
            }
        });

        j12.setEnabled(false);
        j12.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j12FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j12FocusLost(evt);
            }
        });

        j13.setEnabled(false);
        j13.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j13FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j13FocusLost(evt);
            }
        });

        j03.setEnabled(false);
        j03.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                j03FocusLost(evt);
            }
        });
        j03.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                j03ActionPerformed(evt);
            }
        });

        j02.setEnabled(false);
        j02.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                j02FocusLost(evt);
            }
        });

        j01.setEnabled(false);
        j01.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                j01FocusLost(evt);
            }
        });

        j00.setEnabled(false);
        j00.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j00FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j00FocusLost(evt);
            }
        });

        j20.setEnabled(false);
        j20.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j20FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j20FocusLost(evt);
            }
        });

        j21.setEnabled(false);
        j21.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j21FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j21FocusLost(evt);
            }
        });

        j22.setEnabled(false);
        j22.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j22FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j22FocusLost(evt);
            }
        });

        j23.setEnabled(false);
        j23.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j23FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j23FocusLost(evt);
            }
        });

        j04.setEnabled(false);
        j04.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j04FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j04FocusLost(evt);
            }
        });

        j14.setEnabled(false);
        j14.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j14FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j14FocusLost(evt);
            }
        });

        j24.setEnabled(false);
        j24.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j24FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j24FocusLost(evt);
            }
        });

        j05.setEnabled(false);
        j05.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                j05FocusLost(evt);
            }
        });

        j15.setEnabled(false);
        j15.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j15FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j15FocusLost(evt);
            }
        });
        j15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                j15ActionPerformed(evt);
            }
        });

        j25.setEnabled(false);
        j25.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j25FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j25FocusLost(evt);
            }
        });

        j06.setEnabled(false);
        j06.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                j06FocusLost(evt);
            }
        });

        j16.setEnabled(false);
        j16.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j16FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j16FocusLost(evt);
            }
        });

        j26.setEnabled(false);
        j26.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j26FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j26FocusLost(evt);
            }
        });

        j07.setEnabled(false);
        j07.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                j07FocusLost(evt);
            }
        });
        j07.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                j07ActionPerformed(evt);
            }
        });

        j17.setEnabled(false);
        j17.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j17FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j17FocusLost(evt);
            }
        });

        j27.setEnabled(false);
        j27.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j27FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j27FocusLost(evt);
            }
        });

        support.setBackground(new java.awt.Color(0, 0, 102));
        support.setEnabled(false);
        support.setLabel("");

        brasVisible.setEditable(false);
        brasVisible.setOpaque(false);
        brasVisible.setRequestFocusEnabled(false);

        fil.setBackground(new java.awt.Color(255, 204, 204));
        fil.setEnabled(false);
        fil.setLabel("");

        j30.setEditable(false);

        j31.setEditable(false);

        j32.setEditable(false);

        j33.setEditable(false);

        j34.setEditable(false);

        j35.setEditable(false);

        j36.setEditable(false);

        j37.setEditable(false);
        j37.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                j37ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pan4Layout = new javax.swing.GroupLayout(pan4);
        pan4.setLayout(pan4Layout);
        pan4Layout.setHorizontalGroup(
            pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan4Layout.createSequentialGroup()
                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pan4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan4Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(j00, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(j01, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(j02, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(j03, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(j04, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan4Layout.createSequentialGroup()
                                .addComponent(j10, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(j11, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(j12, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(j13, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(j14, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pan4Layout.createSequentialGroup()
                                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pan4Layout.createSequentialGroup()
                                        .addComponent(j20, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(j21, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(pan4Layout.createSequentialGroup()
                                        .addComponent(j30, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(j31, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(j22, javax.swing.GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE)
                                    .addComponent(j32))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(j23, javax.swing.GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE)
                                    .addComponent(j33))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(j24, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(j34, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(pan4Layout.createSequentialGroup()
                        .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pan4Layout.createSequentialGroup()
                                .addGap(332, 332, 332)
                                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pan4Layout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addComponent(fil, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(support, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(pan4Layout.createSequentialGroup()
                                .addGap(306, 306, 306)
                                .addComponent(brasVisible, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pan4Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pan4Layout.createSequentialGroup()
                                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(j05, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                                    .addComponent(j15))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(j16)
                                    .addComponent(j06)))
                            .addGroup(pan4Layout.createSequentialGroup()
                                .addComponent(j25, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(j26, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(pan4Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(j35, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(j36, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(j17, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pan4Layout.createSequentialGroup()
                        .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(j07, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(j37, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE)
                                .addComponent(j27, javax.swing.GroupLayout.Alignment.LEADING)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pan4Layout.setVerticalGroup(
            pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan4Layout.createSequentialGroup()
                .addComponent(support, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(fil, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(brasVisible, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(j31, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                    .addComponent(j30)
                    .addComponent(j32)
                    .addComponent(j33)
                    .addComponent(j34)
                    .addComponent(j35)
                    .addComponent(j36)
                    .addComponent(j37))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(j21, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(j23, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(j24, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(j25, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(j26, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(j27, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(j20)
                    .addComponent(j22, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(j10)
                    .addComponent(j11)
                    .addComponent(j13)
                    .addComponent(j12)
                    .addComponent(j14)
                    .addComponent(j17, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(j15)
                    .addComponent(j16))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(j07, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(j04, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(j05, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(j00)
                    .addComponent(j06)
                    .addComponent(j01)
                    .addComponent(j02, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(j03, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout pan3Layout = new javax.swing.GroupLayout(pan3);
        pan3.setLayout(pan3Layout);
        pan3Layout.setHorizontalGroup(
            pan3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 688, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pan4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pan3Layout.setVerticalGroup(
            pan3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pan4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel11))
        );

        javax.swing.GroupLayout pan2Layout = new javax.swing.GroupLayout(pan2);
        pan2.setLayout(pan2Layout);
        pan2Layout.setHorizontalGroup(
            pan2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pan3, javax.swing.GroupLayout.PREFERRED_SIZE, 712, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24))
        );
        pan2Layout.setVerticalGroup(
            pan2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pan3, javax.swing.GroupLayout.PREFERRED_SIZE, 368, Short.MAX_VALUE))
        );

        simuler.setBackground(new java.awt.Color(204, 255, 204));
        simuler.setEnabled(false);
        simuler.setLabel("Simuler");
        simuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                simulerActionPerformed(evt);
            }
        });

        jPanel6.setBackground(new java.awt.Color(204, 255, 255));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Arial Narrow", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 102, 102));
        jLabel1.setText("Robot Classeur");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 3, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(153, 204, 255));

        jLabel12.setFont(new java.awt.Font("Bookman Old Style", 2, 18)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(51, 51, 51));
        jLabel12.setText("Configuration initiale");

        initiale00.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale00FocusLost(evt);
            }
        });

        initiale10.setEditable(false);
        initiale10.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale10FocusLost(evt);
            }
        });

        initiale20.setEditable(false);
        initiale20.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale20FocusLost(evt);
            }
        });

        initiale21.setEditable(false);
        initiale21.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale21FocusLost(evt);
            }
        });
        initiale21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                initiale21ActionPerformed(evt);
            }
        });

        initiale22.setEditable(false);
        initiale22.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale22FocusLost(evt);
            }
        });

        initiale23.setEditable(false);
        initiale23.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale23FocusLost(evt);
            }
        });

        initiale24.setEditable(false);
        initiale24.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale24FocusLost(evt);
            }
        });
        initiale24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                initiale24ActionPerformed(evt);
            }
        });

        initiale25.setEditable(false);
        initiale25.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale25FocusLost(evt);
            }
        });
        initiale25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                initiale25ActionPerformed(evt);
            }
        });

        initiale26.setEditable(false);
        initiale26.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale26FocusLost(evt);
            }
        });

        initiale11.setEditable(false);
        initiale11.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale11FocusLost(evt);
            }
        });

        initiale01.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale01FocusLost(evt);
            }
        });
        initiale01.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                initiale01ActionPerformed(evt);
            }
        });

        initiale02.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale02FocusLost(evt);
            }
        });
        initiale02.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                initiale02ActionPerformed(evt);
            }
        });

        initiale12.setEditable(false);
        initiale12.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale12FocusLost(evt);
            }
        });

        initiale03.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale03FocusLost(evt);
            }
        });

        initiale04.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale04FocusLost(evt);
            }
        });

        initiale05.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale05FocusLost(evt);
            }
        });

        initiale06.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale06FocusLost(evt);
            }
        });

        initiale07.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale07FocusLost(evt);
            }
        });

        initiale13.setEditable(false);
        initiale13.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale13FocusLost(evt);
            }
        });

        initiale14.setEditable(false);
        initiale14.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale14FocusLost(evt);
            }
        });
        initiale14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                initiale14ActionPerformed(evt);
            }
        });

        initiale15.setEditable(false);
        initiale15.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale15FocusLost(evt);
            }
        });
        initiale15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                initiale15ActionPerformed(evt);
            }
        });

        initiale17.setEditable(false);
        initiale17.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale17FocusLost(evt);
            }
        });

        initiale16.setEditable(false);
        initiale16.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale16FocusLost(evt);
            }
        });

        initiale27.setEditable(false);
        initiale27.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                initiale27FocusLost(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(93, 93, 93)
                        .addComponent(jLabel12))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(initiale20, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                            .addComponent(initiale10, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(initiale00, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(initiale21, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                            .addComponent(initiale11)
                            .addComponent(initiale01))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(initiale22, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                            .addComponent(initiale02)
                            .addComponent(initiale12))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(initiale23, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                            .addComponent(initiale03)
                            .addComponent(initiale13))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(initiale24, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                            .addComponent(initiale04)
                            .addComponent(initiale14))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(initiale25, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                            .addComponent(initiale05)
                            .addComponent(initiale15))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(initiale26, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(initiale27, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(initiale06, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(initiale07, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                .addComponent(initiale16, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(initiale17, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(initiale20, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                    .addComponent(initiale21)
                    .addComponent(initiale22)
                    .addComponent(initiale23)
                    .addComponent(initiale24)
                    .addComponent(initiale25)
                    .addComponent(initiale26)
                    .addComponent(initiale27))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(initiale11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(initiale12, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(initiale13, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(initiale14, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(initiale15, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(initiale16, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(initiale17, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(initiale10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(initiale07, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                    .addComponent(initiale03)
                    .addComponent(initiale02)
                    .addComponent(initiale00)
                    .addComponent(initiale01)
                    .addComponent(initiale04)
                    .addComponent(initiale06)
                    .addComponent(initiale05))
                .addContainerGap())
        );

        jPanel5.setBackground(new java.awt.Color(204, 204, 255));

        jLabel14.setBackground(new java.awt.Color(255, 0, 51));
        jLabel14.setFont(new java.awt.Font("Bookman Old Style", 2, 18)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(0, 102, 153));
        jLabel14.setText("Configuration finale");

        finale00.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale00FocusLost(evt);
            }
        });

        finale10.setEditable(false);
        finale10.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale10FocusLost(evt);
            }
        });

        finale20.setEditable(false);
        finale20.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale20FocusLost(evt);
            }
        });

        finale21.setEditable(false);
        finale21.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale21FocusLost(evt);
            }
        });
        finale21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                finale21ActionPerformed(evt);
            }
        });

        finale22.setEditable(false);
        finale22.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale22FocusLost(evt);
            }
        });
        finale22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                finale22ActionPerformed(evt);
            }
        });

        finale23.setEditable(false);
        finale23.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale23FocusLost(evt);
            }
        });

        finale24.setEditable(false);
        finale24.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale24FocusLost(evt);
            }
        });

        finale25.setEditable(false);
        finale25.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale25FocusLost(evt);
            }
        });

        finale26.setEditable(false);
        finale26.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale26FocusLost(evt);
            }
        });

        finale11.setEditable(false);
        finale11.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale11FocusLost(evt);
            }
        });

        finale01.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale01FocusLost(evt);
            }
        });

        finale02.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale02FocusLost(evt);
            }
        });

        finale12.setEditable(false);
        finale12.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale12FocusLost(evt);
            }
        });

        finale03.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale03FocusLost(evt);
            }
        });

        finale04.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale04FocusLost(evt);
            }
        });

        finale05.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale05FocusLost(evt);
            }
        });

        finale06.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale06FocusLost(evt);
            }
        });

        finale07.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale07FocusLost(evt);
            }
        });

        finale13.setEditable(false);
        finale13.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale13FocusLost(evt);
            }
        });

        finale14.setEditable(false);
        finale14.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale14FocusLost(evt);
            }
        });

        finale15.setEditable(false);
        finale15.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale15FocusLost(evt);
            }
        });

        finale17.setEditable(false);
        finale17.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale17FocusLost(evt);
            }
        });

        finale16.setEditable(false);
        finale16.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale16FocusLost(evt);
            }
        });

        finale27.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                finale27FocusLost(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(93, 93, 93)
                        .addComponent(jLabel14))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(finale20, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                            .addComponent(finale10, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(finale00, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(finale21, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                            .addComponent(finale11)
                            .addComponent(finale01))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(finale22, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                            .addComponent(finale02)
                            .addComponent(finale12))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(finale23, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                            .addComponent(finale03)
                            .addComponent(finale13))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(finale24, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                            .addComponent(finale04)
                            .addComponent(finale14))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(finale25, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                            .addComponent(finale05)
                            .addComponent(finale15))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(finale26, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(finale27, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(finale06, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(finale07, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                                .addComponent(finale16, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(finale17, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(finale27)
                    .addComponent(finale22, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                    .addComponent(finale21)
                    .addComponent(finale20)
                    .addComponent(finale23, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(finale24, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(finale25)
                    .addComponent(finale26))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(finale11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(finale12, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(finale13, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(finale14, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(finale15, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(finale16, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(finale17, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(finale10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(finale07, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                    .addComponent(finale03)
                    .addComponent(finale02)
                    .addComponent(finale00)
                    .addComponent(finale01)
                    .addComponent(finale04)
                    .addComponent(finale06)
                    .addComponent(finale05)))
        );

        jPanel7.setBackground(new java.awt.Color(204, 204, 204));

        jPanel8.setBackground(new java.awt.Color(153, 255, 255));

        jLabel15.setBackground(new java.awt.Color(0, 0, 0));
        jLabel15.setFont(new java.awt.Font("Tw Cen MT Condensed", 1, 24)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(51, 51, 51));
        jLabel15.setText("   Personalisation et vues");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(102, 102, 102))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );

        prog2.setBackground(new java.awt.Color(0, 102, 102));
        prog2.setForeground(new java.awt.Color(0, 255, 204));
        prog2.setMaximum(24);

        prog1.setBackground(new java.awt.Color(51, 255, 153));
        prog1.setForeground(new java.awt.Color(204, 0, 255));

        v.setMaximum(10);
        v.setMinimum(1);
        v.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                vStateChanged(evt);
            }
        });
        v.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                vPropertyChange(evt);
            }
        });

        jLabel16.setBackground(new java.awt.Color(0, 102, 102));
        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(102, 255, 51));
        jLabel16.setText("Vitesse ");

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(0, 0, 51));
        jLabel18.setText("Progrssion du rangement courant...");

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(102, 51, 0));
        jLabel19.setText("Progrssion globale des rangements");

        jLabel21.setFont(new java.awt.Font("Yu Gothic UI Semilight", 1, 18)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(51, 255, 255));
        jLabel21.setText("Transitions");

        ListeTransitions.setModel(transitions);
        jScrollPane3.setViewportView(ListeTransitions);

        jLabel22.setFont(new java.awt.Font("Tw Cen MT", 1, 14)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(51, 153, 0));

        jPanel9.setBackground(new java.awt.Color(102, 102, 102));

        jLabel28.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(255, 255, 255));
        jLabel28.setText("Position définitive");

        jLabel30.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(255, 153, 153));
        jLabel30.setText("Rangement temporaire");

        jLabel24.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(255, 255, 102));
        jLabel24.setText("Pret pour le rangement");

        jLabel26.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(51, 255, 51));
        jLabel26.setText("Position initiale");

        jLabel31.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        jLabel31.setForeground(new java.awt.Color(204, 0, 204));
        jLabel31.setText(" deplacement temporaire");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel31)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(23, 23, 23))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18))))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel26)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel24)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel31)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel30)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel28)
                .addContainerGap())
        );

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(0, 51, 204));
        jLabel25.setText("Legende");

        latt.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        latt.setForeground(new java.awt.Color(0, 102, 102));
        latt.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0", "1", "2", "3", "4", "5", "10", "15" }));
        latt.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                lattFocusLost(evt);
            }
        });
        latt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lattActionPerformed(evt);
            }
        });

        jLabel17.setBackground(new java.awt.Color(0, 102, 102));
        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(21, 28, 83));
        jLabel17.setText("Latence inter-transition(s)");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(v, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(prog2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(prog1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel25)
                        .addGap(79, 79, 79))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel7Layout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addComponent(jLabel17)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(jPanel7Layout.createSequentialGroup()
                                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                                        .addComponent(jLabel22))))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(latt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(77, 77, 77)))))
                .addContainerGap())
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(v, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                    .addComponent(jLabel25))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(latt, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane3))
                .addGap(8, 8, 8)
                .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(prog1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(prog2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
        );

        Load.setBackground(new java.awt.Color(255, 204, 204));
        Load.setForeground(new java.awt.Color(0, 102, 102));
        Load.setText("Load");
        Load.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LoadActionPerformed(evt);
            }
        });

        reload.setBackground(new java.awt.Color(255, 153, 153));
        reload.setText("Reload");
        reload.setEnabled(false);
        reload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reloadActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pan1Layout = new javax.swing.GroupLayout(pan1);
        pan1.setLayout(pan1Layout);
        pan1Layout.setHorizontalGroup(
            pan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pan1Layout.createSequentialGroup()
                        .addGroup(pan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pan1Layout.createSequentialGroup()
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pan1Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(pan2, javax.swing.GroupLayout.PREFERRED_SIZE, 729, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pan1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(119, 119, 119)))
                .addGroup(pan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pan1Layout.createSequentialGroup()
                        .addComponent(Load, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(reload, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)
                        .addComponent(simuler, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pan1Layout.setVerticalGroup(
            pan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(simuler, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                    .addGroup(pan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(Load, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                        .addComponent(reload, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pan1Layout.createSequentialGroup()
                        .addGroup(pan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 160, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pan2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jPanel12.setBackground(new java.awt.Color(0, 102, 102));

        jLabel3.setFont(new java.awt.Font("Rockwell Condensed", 1, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("TP d'algorithmique 2020/2021");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(478, 478, 478)
                .addComponent(jLabel3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel12, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pan1, javax.swing.GroupLayout.PREFERRED_SIZE, 1219, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pan1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void j11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_j11ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_j11ActionPerformed

    private void j03ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_j03ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_j03ActionPerformed
  
    private void j00FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j00FocusLost
        
    }//GEN-LAST:event_j00FocusLost

    private void j00FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j00FocusGained
         
        
    }//GEN-LAST:event_j00FocusGained

    private void j01FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j01FocusLost
       
    }//GEN-LAST:event_j01FocusLost

    private void j11FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j11FocusLost
        
    }//GEN-LAST:event_j11FocusLost

    private void j21FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j21FocusGained
     
    }//GEN-LAST:event_j21FocusGained

    private void j10FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j10FocusLost
       
    }//GEN-LAST:event_j10FocusLost

    private void j12FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j12FocusLost

    }//GEN-LAST:event_j12FocusLost

    private void j22FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j22FocusLost
        
    }//GEN-LAST:event_j22FocusLost

    private void j13FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j13FocusLost
        
    }//GEN-LAST:event_j13FocusLost

    private void j03FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j03FocusLost
        
    }//GEN-LAST:event_j03FocusLost

    private void j02FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j02FocusLost
       
    }//GEN-LAST:event_j02FocusLost

    private void j20FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j20FocusLost
        
    }//GEN-LAST:event_j20FocusLost

    private void j21FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j21FocusLost
        
    }//GEN-LAST:event_j21FocusLost

    private void j23FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j23FocusLost
      
    }//GEN-LAST:event_j23FocusLost

    private void j14FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j14FocusLost
       
    }//GEN-LAST:event_j14FocusLost

    private void j15FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j15FocusLost
        
    }//GEN-LAST:event_j15FocusLost

    private void j15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_j15ActionPerformed
        
    }//GEN-LAST:event_j15ActionPerformed

    private void j16FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j16FocusLost
        
    }//GEN-LAST:event_j16FocusLost

    private void j17FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j17FocusLost
       
    }//GEN-LAST:event_j17FocusLost

    private void j07FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j07FocusLost
       
    }//GEN-LAST:event_j07FocusLost

    private void j07ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_j07ActionPerformed
         
    }//GEN-LAST:event_j07ActionPerformed

    private void j06FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j06FocusLost
       
    }//GEN-LAST:event_j06FocusLost

    private void j05FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j05FocusLost
       
    }//GEN-LAST:event_j05FocusLost

    private void j04FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j04FocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_j04FocusGained

    private void j04FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j04FocusLost
       
    }//GEN-LAST:event_j04FocusLost

    private void j24FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j24FocusLost
       
    }//GEN-LAST:event_j24FocusLost

    private void j25FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j25FocusGained
        
    }//GEN-LAST:event_j25FocusGained

    private void j25FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j25FocusLost
       
    }//GEN-LAST:event_j25FocusLost

    private void j26FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j26FocusLost
       
    }//GEN-LAST:event_j26FocusLost

    private void j27FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j27FocusLost
       
    }//GEN-LAST:event_j27FocusLost

    private void simulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_simulerActionPerformed
         cont = new Thread(new Simulation.deplacer());
         cont.start();
         simuler.setEnabled(false);
         reload.setEnabled(true);
    }//GEN-LAST:event_simulerActionPerformed

    private void j10FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j10FocusGained
       
    }//GEN-LAST:event_j10FocusGained

    private void j11FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j11FocusGained
        
    }//GEN-LAST:event_j11FocusGained

    private void j12FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j12FocusGained
        
    }//GEN-LAST:event_j12FocusGained

    private void j13FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j13FocusGained
       
    }//GEN-LAST:event_j13FocusGained

    private void j20FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j20FocusGained
         
    }//GEN-LAST:event_j20FocusGained

    private void j22FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j22FocusGained
        
    }//GEN-LAST:event_j22FocusGained

    private void j23FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j23FocusGained
         
    }//GEN-LAST:event_j23FocusGained

    private void j14FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j14FocusGained
        
    }//GEN-LAST:event_j14FocusGained

    private void j15FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j15FocusGained
        
    }//GEN-LAST:event_j15FocusGained

    private void j16FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j16FocusGained
        
    }//GEN-LAST:event_j16FocusGained

    private void j17FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j17FocusGained
         
    }//GEN-LAST:event_j17FocusGained

    private void j27FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j27FocusGained
        
    }//GEN-LAST:event_j27FocusGained

    private void j26FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j26FocusGained
        
    }//GEN-LAST:event_j26FocusGained

    private void j24FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j24FocusGained
         
    }//GEN-LAST:event_j24FocusGained

    private void j37ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_j37ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_j37ActionPerformed

    private void initiale01ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_initiale01ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_initiale01ActionPerformed

    private void initiale02ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_initiale02ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_initiale02ActionPerformed

    private void initiale21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_initiale21ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_initiale21ActionPerformed

    private void initiale14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_initiale14ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_initiale14ActionPerformed

    private void vPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_vPropertyChange
        v.setValue(5);
        vitesse=v.getMaximum()-v.getValue();
    }//GEN-LAST:event_vPropertyChange

    private void vStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_vStateChanged

        vitesse=v.getMaximum()-v.getValue();
    }//GEN-LAST:event_vStateChanged

    private void initiale00FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale00FocusLost

        checkRepetition(initiale00);
        if(initiales[0][0].getText().equals("")){
            initiales[1][0].setText("");
            initiales[1][0].setEditable(false);
            initiales[2][0].setText("");
            initiales[2][0].setEditable(false);
        }
        else
           
            initiales[1][0].setEditable(true);  
    }//GEN-LAST:event_initiale00FocusLost

    private void initiale01FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale01FocusLost
         checkRepetition(initiales[0][1]);
        if((initiales[0][1].getText()).equals("")){
            initiales[1][1].setText("");
            initiales[1][1].setEditable(false);
            initiales[2][1].setText("");
            initiales[2][1].setEditable(false);
        }
        else
            initiales[1][1].setEditable(true);  
    }//GEN-LAST:event_initiale01FocusLost

    private void initiale02FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale02FocusLost
        checkRepetition(initiales[0][2]);
        if((initiales[0][2].getText()).equals("")){
            initiales[1][2].setText("");
            initiales[1][2].setEditable(false);
            initiales[2][2].setText("");
            initiales[2][2].setEditable(false);
        }
        else
            initiales[1][2].setEditable(true);  
    }//GEN-LAST:event_initiale02FocusLost

    private void initiale03FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale03FocusLost
         checkRepetition(initiales[0][3]);
        if((initiales[0][3].getText()).equals("")){
            initiales[1][3].setText("");
            initiales[1][3].setEditable(false);
            initiales[2][3].setText("");
            initiales[2][3].setEditable(false);
        }
        else
            initiales[1][3].setEditable(true);  
    }//GEN-LAST:event_initiale03FocusLost

    private void initiale04FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale04FocusLost
         checkRepetition(initiales[0][4]);
        if((initiales[0][4].getText()).equals("")){
            initiales[1][4].setText("");
            initiales[1][4].setEditable(false);
            initiales[2][4].setText("");
            initiales[2][4].setEditable(false);
        }
        else
            initiales[1][4].setEditable(true);  
    }//GEN-LAST:event_initiale04FocusLost

    private void initiale05FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale05FocusLost
         checkRepetition(initiales[0][5]);
        if((initiales[0][5].getText()).equals("")){
            initiales[1][5].setText("");
            initiales[1][5].setEditable(false);
            initiales[2][5].setText("");
            initiales[2][5].setEditable(false);
        }
        else
            initiales[1][5].setEditable(true);  
    }//GEN-LAST:event_initiale05FocusLost

    private void initiale06FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale06FocusLost
        checkRepetition(initiales[0][6]);
        if((initiales[0][6].getText()).equals("")){
            initiales[1][6].setText("");
            initiales[1][6].setEditable(false);
            initiales[2][6].setText("");
            initiales[2][6].setEditable(false);
        }
        else
            initiales[1][6].setEditable(true);  
    }//GEN-LAST:event_initiale06FocusLost

    private void initiale07FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale07FocusLost
         checkRepetition(initiales[0][7]);
        if((initiales[0][7].getText()).equals("")){
            initiales[1][7].setText("");
            initiales[1][7].setEditable(false);
            initiales[2][7].setText("");
            initiales[2][7].setEditable(false);
        }
        else
            
            initiales[1][7].setEditable(true);  
    }//GEN-LAST:event_initiale07FocusLost

    private void initiale17FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale17FocusLost
        checkRepetition(initiales[1][7]);
        if(initiales[1][7].getText().equals("")){
            initiales[2][7].setText("");
            initiales[2][7].setEditable(false);
        }
        else
            
           initiales[2][7].setEditable(true);
    }//GEN-LAST:event_initiale17FocusLost

    private void initiale16FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale16FocusLost
        checkRepetition(initiales[1][6]);
        if(initiales[1][6].getText().equals("")){
            initiales[2][6].setText("");
            initiales[2][6].setEditable(false);
        }
        else
           
           initiales[2][6].setEditable(true);
    }//GEN-LAST:event_initiale16FocusLost

    private void initiale15FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale15FocusLost
        checkRepetition(initiales[1][5]);
        if(initiales[1][5].getText().equals("")){
            initiales[2][5].setText("");
            initiales[2][5].setEditable(false);
        }
        else
            
            initiales[2][5].setEditable(true);
    }//GEN-LAST:event_initiale15FocusLost

    private void initiale14FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale14FocusLost
        checkRepetition(initiales[1][4]);
        if(initiales[1][4].getText().equals("")){
            initiales[2][4].setText("");
            initiales[2][4].setEditable(false);
        }
        else
           
            initiales[2][4].setEditable(true);
    }//GEN-LAST:event_initiale14FocusLost

    private void initiale13FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale13FocusLost
        checkRepetition(initiales[1][3]);
        if(initiales[1][3].getText().equals("")){
            initiales[2][3].setText("");
            initiales[2][3].setEditable(false);
        }
        else
            
            initiales[2][3].setEditable(true);
    }//GEN-LAST:event_initiale13FocusLost

    private void initiale12FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale12FocusLost
        checkRepetition(initiales[1][2]);
        if(initiales[1][2].getText().equals("")){
            initiales[2][2].setText("");
            initiales[2][2].setEditable(false);
        }
        else
            
        initiales[2][2].setEditable(true);
    }//GEN-LAST:event_initiale12FocusLost

    private void initiale11FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale11FocusLost
       checkRepetition(initiales[1][1]);
        if(initiales[1][1].getText().equals("")){
            initiales[2][1].setText("");
            initiales[2][1].setEditable(false);
        }
        else
            
            initiales[2][1].setEditable(true);
    }//GEN-LAST:event_initiale11FocusLost

    private void initiale10FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale10FocusLost
        checkRepetition(initiales[1][0]);
        if(initiales[1][0].getText().equals("")){
            initiales[2][0].setText("");
            initiales[2][0].setEditable(false);
        }
        else
            
            initiales[2][0].setEditable(true);
    }//GEN-LAST:event_initiale10FocusLost

    private void initiale20FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale20FocusLost
         checkRepetition(initiales[2][0]);
        if( initiales[1][0].getText().equals("")) 
             initiales[2][0].setEditable(false);
    }//GEN-LAST:event_initiale20FocusLost

    private void initiale21FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale21FocusLost
         checkRepetition(initiales[2][1]); 
        if( initiales[1][1].getText().equals("")) 
             initiales[2][1].setEditable(false);
    }//GEN-LAST:event_initiale21FocusLost

    private void initiale22FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale22FocusLost
         checkRepetition(initiales[2][2]); 
        if( initiales[1][2].getText().equals("")) 
             initiales[2][2].setEditable(false);
    }//GEN-LAST:event_initiale22FocusLost

    private void initiale23FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale23FocusLost
         checkRepetition(initiales[2][3]);
        if( initiales[1][3].getText().equals("")) 
             initiales[2][3].setEditable(false);
    }//GEN-LAST:event_initiale23FocusLost

    private void initiale24FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale24FocusLost
         checkRepetition(initiales[2][4]);
        if( initiales[1][4].getText().equals("")) 
             initiales[2][4].setEditable(false);
    }//GEN-LAST:event_initiale24FocusLost

    private void initiale25FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale25FocusLost
         checkRepetition(initiales[2][5]); 
        if( initiales[1][5].getText().equals("")) 
             initiales[2][5].setEditable(false);
    }//GEN-LAST:event_initiale25FocusLost

    private void initiale26FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale26FocusLost
         checkRepetition(initiales[2][6]);
        if( initiales[1][6].getText().equals("")) 
             initiales[2][6].setEditable(false);
    }//GEN-LAST:event_initiale26FocusLost

    private void finale00FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale00FocusLost
        checkRepetitionF(finales[0][0]);
        if(finales[0][0].getText().equals("")){
            finales[1][0].setText("");
            finales[1][0].setEditable(false);
            finales[2][0].setText("");
            finales[2][0].setEditable(false);
        }
        else
           
            finales[1][0].setEditable(true);  
    }//GEN-LAST:event_finale00FocusLost

    private void finale01FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale01FocusLost
         checkRepetitionF(finales[0][1]);
        if((finales[0][1].getText()).equals("")){
            finales[1][1].setText("");
            finales[1][1].setEditable(false);
            finales[2][1].setText("");
            finales[2][1].setEditable(false);
        }
        else
            finales[1][1].setEditable(true); 
    }//GEN-LAST:event_finale01FocusLost

    private void finale02FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale02FocusLost
        checkRepetitionF(finales[0][2]);
        if((finales[0][2].getText()).equals("")){
            finales[1][2].setText("");
            finales[1][2].setEditable(false);
            finales[2][2].setText("");
            finales[2][2].setEditable(false);
        }
        else
            finales[1][2].setEditable(true);  
    }//GEN-LAST:event_finale02FocusLost

    private void finale03FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale03FocusLost
        checkRepetitionF(finales[0][3]);
        if((finales[0][3].getText()).equals("")){
            finales[1][3].setText("");
            finales[1][3].setEditable(false);
            finales[2][3].setText("");
            finales[2][3].setEditable(false);
        }
        else
            finales[1][3].setEditable(true);  
    }//GEN-LAST:event_finale03FocusLost

    private void finale04FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale04FocusLost
        checkRepetitionF(finales[0][4]);
        if((finales[0][4].getText()).equals("")){
            finales[1][4].setText("");
            finales[1][4].setEditable(false);
            finales[2][4].setText("");
            finales[2][4].setEditable(false);
        }
        else
            finales[1][4].setEditable(true);  
    }//GEN-LAST:event_finale04FocusLost

    private void finale05FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale05FocusLost
        checkRepetitionF(finales[0][5]);
        if((finales[0][5].getText()).equals("")){
            finales[1][5].setText("");
            finales[1][5].setEditable(false);
            finales[2][5].setText("");
            finales[2][5].setEditable(false);
        }
        else
            finales[1][5].setEditable(true);  
    }//GEN-LAST:event_finale05FocusLost

    private void finale06FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale06FocusLost
        checkRepetitionF(finales[0][6]);
        if((finales[0][6].getText()).equals("")){
            finales[1][6].setText("");
            finales[1][6].setEditable(false);
            finales[2][6].setText("");
            finales[2][6].setEditable(false);
        }
        else
            finales[1][6].setEditable(true);  
    }//GEN-LAST:event_finale06FocusLost

    private void finale07FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale07FocusLost
       checkRepetitionF(finales[0][7]);
        if((finales[0][7].getText()).equals("")){
            finales[1][7].setText("");
            finales[1][7].setEditable(false);
            finales[2][7].setText("");
            finales[2][7].setEditable(false);
        }
        else
            
            finales[1][7].setEditable(true); 
    }//GEN-LAST:event_finale07FocusLost
     private static boolean valide(){
       
        for(int i=0; i<3; i++){
            for(int j=0; j<8; j++){ 
                if(!initiales[i][j].getText().equals("")){
                    boolean rang=false;
                    for(int k=0; k<3; k++){
                         for(int l=0; l<8; l++){
                            if(initiales[i][j].getText().equals(finales[k][l].getText())){
                              rang=true;
                            }
                        } 
                    }
                    if(rang==false) return false;
                }     
            } 
        }
        return true;
    }
    private void LoadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LoadActionPerformed
        stop=false;
        int response= new JOptionPane().showConfirmDialog(this,"Passer à la simulation ?","Confirmation",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);   
        if(response==JOptionPane.NO_OPTION){
              return;
          }
        if(valide()){
            
            arranger();
            simuler.setEnabled(true);
            Load.setEnabled(false);
        }
        else
            JOptionPane.showMessageDialog(this, "Veillez ranger tous les conteneurs","Erreur de validation",0);
        
    }//GEN-LAST:event_LoadActionPerformed

    private void lattActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lattActionPerformed
       latence = (latt.getItemCount());
    }//GEN-LAST:event_lattActionPerformed

    private void initiale27FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_initiale27FocusLost
        checkRepetitionF(finales[2][7]); 
        if( finales[1][7].getText().equals("")) 
             finales[2][7].setEditable(false);
    }//GEN-LAST:event_initiale27FocusLost

    private void finale10FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale10FocusLost
       checkRepetitionF(finales[1][0]);
        if(finales[1][0].getText().equals("")){
            finales[2][0].setText("");
            finales[2][0].setEditable(false);
        }
        else
            
            finales[2][0].setEditable(true);
    }//GEN-LAST:event_finale10FocusLost

    private void finale11FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale11FocusLost
        checkRepetitionF(finales[1][1]);
        if(finales[1][1].getText().equals("")){
            finales[2][1].setText("");
            finales[2][1].setEditable(false);
        }
        else
            
            finales[2][1].setEditable(true);
    }//GEN-LAST:event_finale11FocusLost

    private void finale12FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale12FocusLost
        checkRepetitionF(finales[1][2]);
        if(finales[1][2].getText().equals("")){
            finales[2][2].setText("");
            finales[2][2].setEditable(false);
        }
        else
            
        finales[2][2].setEditable(true);
    }//GEN-LAST:event_finale12FocusLost

    private void finale13FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale13FocusLost
        checkRepetitionF(finales[1][3]);
        if(finales[1][3].getText().equals("")){
            finales[2][3].setText("");
            finales[2][3].setEditable(false);
        }
        else
            
            finales[2][3].setEditable(true);
    }//GEN-LAST:event_finale13FocusLost

    private void finale14FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale14FocusLost
        checkRepetitionF(finales[1][4]);
        if(finales[1][4].getText().equals("")){
            finales[2][4].setText("");
            finales[2][4].setEditable(false);
        }
        else
           
            finales[2][4].setEditable(true);
    }//GEN-LAST:event_finale14FocusLost

    private void finale15FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale15FocusLost
        checkRepetitionF(finales[1][5]);
        if(finales[1][5].getText().equals("")){
            finales[2][5].setText("");
            finales[2][5].setEditable(false);
        }
        else
            
            finales[2][5].setEditable(true);
    }//GEN-LAST:event_finale15FocusLost

    private void finale16FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale16FocusLost
        checkRepetitionF(finales[1][6]);
        if(finales[1][6].getText().equals("")){
            finales[2][6].setText("");
            finales[2][6].setEditable(false);
        }
        else
           
           finales[2][6].setEditable(true);
    }//GEN-LAST:event_finale16FocusLost

    private void finale17FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale17FocusLost
        checkRepetitionF(finales[1][7]);
        if(finales[1][7].getText().equals("")){
            finales[2][7].setText("");
            finales[2][7].setEditable(false);
        }
        else
            
           finales[2][7].setEditable(true);
    }//GEN-LAST:event_finale17FocusLost

    private void finale26FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale26FocusLost
       checkRepetitionF(finales[2][6]);
        if( finales[1][6].getText().equals("")) 
             finales[2][6].setEditable(false);
    }//GEN-LAST:event_finale26FocusLost

    private void finale25FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale25FocusLost
        checkRepetitionF(finales[2][5]); 
        if( finales[1][5].getText().equals("")) 
             finales[2][5].setEditable(false);
    }//GEN-LAST:event_finale25FocusLost

    private void finale24FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale24FocusLost
         checkRepetitionF(finales[2][4]);
        if( finales[1][4].getText().equals("")) 
             finales[2][4].setEditable(false);
    }//GEN-LAST:event_finale24FocusLost

    private void finale23FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale23FocusLost
        checkRepetitionF(finales[2][3]);
        if( finales[1][3].getText().equals("")) 
             finales[2][3].setEditable(false);
    }//GEN-LAST:event_finale23FocusLost

    private void finale22FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale22FocusLost
        checkRepetitionF(finales[2][2]); 
        if( finales[1][2].getText().equals("")) 
             finales[2][2].setEditable(false);
    }//GEN-LAST:event_finale22FocusLost

    private void finale21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_finale21ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_finale21ActionPerformed

    private void finale21FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale21FocusLost
       checkRepetitionF(finales[2][1]); 
        if( finales[1][1].getText().equals("")) 
             finales[2][1].setEditable(false);
    }//GEN-LAST:event_finale21FocusLost

    private void finale20FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale20FocusLost
        checkRepetitionF(finales[2][0]);
        if( finales[1][0].getText().equals("")) 
             finales[2][0].setEditable(false);
    }//GEN-LAST:event_finale20FocusLost

    private void finale27FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_finale27FocusLost
       checkRepetitionF(initiales[2][7]);
        if( initiales[1][7].getText().equals("")) 
             initiales[2][7].setEditable(false);
    }//GEN-LAST:event_finale27FocusLost

    private void initiale25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_initiale25ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_initiale25ActionPerformed

    private void finale22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_finale22ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_finale22ActionPerformed

    private void initiale24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_initiale24ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_initiale24ActionPerformed

    private void initiale15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_initiale15ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_initiale15ActionPerformed

    private void lattFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_lattFocusLost
        latence = latt.getItemCount();
    }//GEN-LAST:event_lattFocusLost

    private void reloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reloadActionPerformed
        int response= new JOptionPane().showConfirmDialog(this,"Attention, le rangement courant risque d'être interrompu. souhaitez vous confirmer votre action? ?","Confirmation",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);   
        if(response==JOptionPane.NO_OPTION){
              return;
          }
        else{ 
            stop=true;
            Load.setEnabled(true);
            reload.setEnabled(false);
            } 
    }//GEN-LAST:event_reloadActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Simulation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Simulation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Simulation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Simulation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Simulation().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JList<String> ListeTransitions;
    private javax.swing.JToggleButton Load;
    public static javax.swing.JLabel Valeur;
    private javax.swing.JFormattedTextField brasVisible;
    private javax.swing.JLabel ec;
    private java.awt.Button fil;
    private javax.swing.JFormattedTextField finale00;
    private javax.swing.JFormattedTextField finale01;
    private javax.swing.JFormattedTextField finale02;
    private javax.swing.JFormattedTextField finale03;
    private javax.swing.JFormattedTextField finale04;
    private javax.swing.JFormattedTextField finale05;
    private javax.swing.JFormattedTextField finale06;
    private javax.swing.JFormattedTextField finale07;
    private javax.swing.JFormattedTextField finale10;
    private javax.swing.JFormattedTextField finale11;
    private javax.swing.JFormattedTextField finale12;
    private javax.swing.JFormattedTextField finale13;
    private javax.swing.JFormattedTextField finale14;
    private javax.swing.JFormattedTextField finale15;
    private javax.swing.JFormattedTextField finale16;
    private javax.swing.JFormattedTextField finale17;
    private javax.swing.JFormattedTextField finale20;
    private javax.swing.JFormattedTextField finale21;
    private javax.swing.JFormattedTextField finale22;
    private javax.swing.JFormattedTextField finale23;
    private javax.swing.JFormattedTextField finale24;
    private javax.swing.JFormattedTextField finale25;
    private javax.swing.JFormattedTextField finale26;
    private javax.swing.JFormattedTextField finale27;
    private javax.swing.JFormattedTextField initiale00;
    private javax.swing.JFormattedTextField initiale01;
    private javax.swing.JFormattedTextField initiale02;
    private javax.swing.JFormattedTextField initiale03;
    private javax.swing.JFormattedTextField initiale04;
    private javax.swing.JFormattedTextField initiale05;
    private javax.swing.JFormattedTextField initiale06;
    private javax.swing.JFormattedTextField initiale07;
    private javax.swing.JFormattedTextField initiale10;
    private javax.swing.JFormattedTextField initiale11;
    private javax.swing.JFormattedTextField initiale12;
    private javax.swing.JFormattedTextField initiale13;
    private javax.swing.JFormattedTextField initiale14;
    private javax.swing.JFormattedTextField initiale15;
    private javax.swing.JFormattedTextField initiale16;
    private javax.swing.JFormattedTextField initiale17;
    private javax.swing.JFormattedTextField initiale20;
    private javax.swing.JFormattedTextField initiale21;
    private javax.swing.JFormattedTextField initiale22;
    private javax.swing.JFormattedTextField initiale23;
    private javax.swing.JFormattedTextField initiale24;
    private javax.swing.JFormattedTextField initiale25;
    private javax.swing.JFormattedTextField initiale26;
    private javax.swing.JFormattedTextField initiale27;
    public javax.swing.JFormattedTextField j00;
    private javax.swing.JFormattedTextField j01;
    private javax.swing.JFormattedTextField j02;
    private javax.swing.JFormattedTextField j03;
    private javax.swing.JFormattedTextField j04;
    private javax.swing.JFormattedTextField j05;
    private javax.swing.JFormattedTextField j06;
    private javax.swing.JFormattedTextField j07;
    private javax.swing.JFormattedTextField j10;
    private javax.swing.JFormattedTextField j11;
    private javax.swing.JFormattedTextField j12;
    private javax.swing.JFormattedTextField j13;
    private javax.swing.JFormattedTextField j14;
    private javax.swing.JFormattedTextField j15;
    private javax.swing.JFormattedTextField j16;
    private javax.swing.JFormattedTextField j17;
    private javax.swing.JFormattedTextField j20;
    private javax.swing.JFormattedTextField j21;
    private javax.swing.JFormattedTextField j22;
    private javax.swing.JFormattedTextField j23;
    private javax.swing.JFormattedTextField j24;
    private javax.swing.JFormattedTextField j25;
    private javax.swing.JFormattedTextField j26;
    private javax.swing.JFormattedTextField j27;
    private javax.swing.JFormattedTextField j30;
    private javax.swing.JFormattedTextField j31;
    private javax.swing.JFormattedTextField j32;
    private javax.swing.JFormattedTextField j33;
    private javax.swing.JFormattedTextField j34;
    private javax.swing.JFormattedTextField j35;
    private javax.swing.JFormattedTextField j36;
    private javax.swing.JFormattedTextField j37;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollBar jScrollBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextPane jTextPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JLabel l00;
    private javax.swing.JLabel l01;
    private javax.swing.JLabel l02;
    private javax.swing.JLabel l03;
    private javax.swing.JLabel l04;
    private javax.swing.JLabel l05;
    private javax.swing.JLabel l06;
    private javax.swing.JLabel l07;
    private javax.swing.JLabel l10;
    private javax.swing.JLabel l11;
    private javax.swing.JLabel l12;
    private javax.swing.JLabel l13;
    private javax.swing.JLabel l14;
    private javax.swing.JLabel l15;
    private javax.swing.JLabel l16;
    private javax.swing.JLabel l17;
    private javax.swing.JLabel l20;
    private javax.swing.JLabel l21;
    private javax.swing.JLabel l22;
    private javax.swing.JLabel l23;
    private javax.swing.JLabel l24;
    private javax.swing.JLabel l25;
    private javax.swing.JLabel l26;
    private javax.swing.JLabel l27;
    private javax.swing.JComboBox<String> latt;
    private javax.swing.JLabel nbTransition;
    private javax.swing.JPanel pan1;
    private javax.swing.JPanel pan2;
    private javax.swing.JPanel pan3;
    private javax.swing.JPanel pan4;
    private javax.swing.JProgressBar prog1;
    private javax.swing.JProgressBar prog2;
    private javax.swing.JToggleButton reload;
    private java.awt.Button simuler;
    private java.awt.Button support;
    private javax.swing.JSlider v;
    // End of variables declaration//GEN-END:variables
    private DefaultListModel transitions = new DefaultListModel();
    private static javax.swing.JFormattedTextField[][] conteneurs= new javax.swing.JFormattedTextField[4][8];
    private static javax.swing.JFormattedTextField[][] finales= new javax.swing.JFormattedTextField[4][8];
    private static javax.swing.JFormattedTextField[][] initiales= new javax.swing.JFormattedTextField[4][8];
    private Thread cont;
    private int Xpositions[][] = new int[4][8];
    private int Ypositions[][] = new int[4][8];
    Graphics2D br_su;
    private static javax.swing.JFormattedTextField bras = new javax.swing.JFormattedTextField();
    private static int transit = 0, vitesse=2;
    private static int latence=0;
    boolean stop=false;
    class deplacer implements Runnable{
        
        deplacer(){
          
        }
        
        public void run(){
            int  yb=0;
                 //Le bras s'en va à la recherche du conteneur 
            for(int p=0; p<3; p++){
                for(int q=0; q<8; q++){
                    if(!finales[p][q].getText().equals("")){
                        yb=q; break;
                    } 
                }
            }   
        positionerBrasNormal(3, 4, 3, yb);
            for(int i=0; i<3; i++){
                for(int j=0; j<8; j++){
                   if(stop)break;
                   if(!finales[i][j].getText().equals("") && !conteneurs[i][j].getText().equals(finales[i][j].getText())){
                       transitions.addElement("");
                       transitions.addElement("Préparation de "+finales[i][j].getText());
                       System.out.println("Préparation de "+finales[i][j].getText());
                       transitions.addElement("");
                       int xd=0, yd=0;
                        for(int l=0; l<3; l++){
                            for(int m=0; m<8; m++){
                               if(conteneurs[l][m].getText().equals(finales[i][j].getText())){
                                   xd=l;
                                   yd=m;
                               }
                            }
                        }
                        transitions.addElement("");
                        transitions.addElement("Liberation du chemin");
                        transitions.addElement("");
                        conteneurs[xd][yd].setBackground(Color.YELLOW);
                        int p=2;
                        
                        while(p>xd){
                            deplacementTemp(p,yd,yd,j);
                            p--;
                        }
                        
                        p=2;
                        
                        while(p>=i){
                            deplacementTemp(p,j,yd,j);
                            p--;
                        }
                       
                        for(int l=0; l<3; l++){
                            for(int m=0; m<8; m++){
                               if(conteneurs[l][m].getText().equals(finales[i][j].getText())){
                                   xd=l;
                                   yd=m;
                                   conteneurs[l][m].setBackground(Color.YELLOW);
                               }
                            }
                        }
                        
                        transitions.addElement("");
                        transitions.addElement("Debut de "+finales[i][j].getText());
                        System.out.println("Debut de "+finales[i][j].getText());
                        transitions.addElement("");
                        deplacementComplet(xd, yd,i,j);
                        transitions.addElement("");
                        transitions.addElement("Fin "+conteneurs[i][j].getText());
                        System.out.println("Fin "+conteneurs[i][j].getText());
                        transitions.addElement("");
                    } 
                   
                   prog1.setValue(prog1.getValue()+1);
                   prog2.setValue(prog2.getValue()+1);
                   conteneurs[i][j].setBackground(Color.WHITE);
                }            
            }
            prog1.setValue(0);
            initialiserBras();
            Load.setEnabled(true);
            reload.setEnabled(false);
            
            cont.stop();
        }
        
    }
    private void deplacementTemp(int x, int y, int a, int b){
        if(!conteneurs[x][y].getText().equals("")){
            conteneurs[x][y].setBackground(Color.MAGENTA);

            int xMin=24, yMin=88;
            for(int i=0; i<3; i++){
                for(int j=0; j<8; j++){
                    if(conteneurs[i][j].getText().equals("") && j!=b && j!=a){
                        if(i>0){
                            if(!conteneurs[i-1][j].getText().equals("") && 
                                Math.sqrt(Math.pow(y-j,2)+Math.pow(x-i,2))<Math.sqrt(Math.pow(y-yMin,2)+Math.pow(x-xMin,2))){
                                xMin=i; yMin=j;
                            }
                        }
                        else if(Math.sqrt(Math.pow(y-j,2)+Math.pow(x-i,2))<Math.sqrt(Math.pow(y-yMin,2)+Math.pow(x-xMin,2))){        
                            xMin=i; yMin=j;
                        }
                    } 
                }
           }
           deplacementComplet(x,y,xMin,yMin);
           conteneurs[xMin][yMin].setBackground(Color.pink);
           if(conteneurs[xMin][yMin].getText().equals(finales[xMin][yMin].getText())) conteneurs[xMin][yMin].setBackground(Color.WHITE);
        }   
    }
    private void initialiserBras(){
        while(bras.getY()>brasVisible.getY()){
            bras.setBounds(bras.getX(),bras.getY()-1,brasVisible.getWidth(),brasVisible.getHeight());
            support.setBounds(bras.getX()+25,support.getY(),support.getWidth(),support.getHeight());
            fil.setBounds(support.getX()+11,support.getY()+support.getHeight(),5,bras.getY()-support.getHeight());
            try{
                cont.sleep(vitesse);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        bras.setBackground(Color.PINK);
    }
    private void deplacement(int xd, int yd, int i, int j){
        
        bras.setBackground(Color.CYAN); 
        Xpositions[xd][yd]=conteneurs[xd][yd].getX();
        Ypositions[xd][yd]=conteneurs[xd][yd].getY();
        Xpositions[i][j]=conteneurs[i][j].getX();
        Ypositions[i][j]=conteneurs[i][j].getY();
        while(conteneurs[xd][yd].getX()!=conteneurs[i][j].getX() || conteneurs[xd][yd].getY()!=conteneurs[i][j].getY()){
            if(Xpositions[i][j]<Xpositions[xd][yd] && Ypositions[i][j]<Ypositions[xd][yd]){
                Xpositions[xd][yd]--; Ypositions[xd][yd]--;
            }
            else if(Xpositions[i][j]<Xpositions[xd][yd] && Ypositions[i][j]>Ypositions[xd][yd]){
                Xpositions[xd][yd]--; Ypositions[xd][yd]++;
            }
            else if(Xpositions[i][j]<Xpositions[xd][yd] && Ypositions[i][j]==Ypositions[xd][yd]){
                Xpositions[xd][yd]--;
            }
            else if(Xpositions[i][j]>Xpositions[xd][yd] && Ypositions[i][j]<Ypositions[xd][yd]){
                Xpositions[xd][yd]++; Ypositions[xd][yd]--;
            }
            else if(Xpositions[i][j]>Xpositions[xd][yd] && Ypositions[i][j]>Ypositions[xd][yd]){
                Xpositions[xd][yd]++; Ypositions[xd][yd]++;
            }
            else if(Xpositions[i][j]>Xpositions[xd][yd] && Ypositions[i][j]==Ypositions[xd][yd]){
                Xpositions[xd][yd]++;
            }
            else if(Xpositions[i][j]==Xpositions[xd][yd] && Ypositions[i][j]<Ypositions[xd][yd]){
                Ypositions[xd][yd]--;
            }
            else if(Xpositions[i][j]==Xpositions[xd][yd] && Ypositions[i][j]>Ypositions[xd][yd]){
                Ypositions[xd][yd]++;
            }
            bras.setBounds(Xpositions[xd][yd],Ypositions[xd][yd],brasVisible.getWidth(),brasVisible.getHeight());
            support.setBounds(bras.getX()+25,support.getY(),support.getWidth(),support.getHeight());
            fil.setBounds(support.getX()+11,support.getY()+support.getHeight(),5,bras.getY()-support.getHeight());
            conteneurs[xd][yd].setBounds(Xpositions[xd][yd],Ypositions[xd][yd],conteneurs[xd][yd].getWidth(),conteneurs[xd][yd].getHeight());
            try{
               // prog1.setValue(prog1.getValue()+1);
                cont.sleep(vitesse);
            }catch(Exception e){
                e.printStackTrace();
            }  
            prog1.setValue(prog1.getValue()+1);
        }
        try{
               cont.sleep(latence*100);
            }catch(Exception e){
                e.printStackTrace();
            }  
         bras.setBackground(Color.orange); 
         
    }
    private void positionerBrasNormal(int xb, int yb, int x, int y){
        
                   
        for(int xd=0; xd<3; xd++){
            for(int yd=0; yd<8; yd++){
                if(!(conteneurs[xd][yd].getX()>=bras.getX() && bras.getX()+bras.getWidth()>=conteneurs[xd][yd].getX()+conteneurs[xd][yd].getWidth() ||
            conteneurs[xd][yd].getY()<=bras.getY() && bras.getY()+bras.getHeight()>=conteneurs[xd][yd].getY()+conteneurs[xd][yd].getHeight())){
                    xb=xd; yb=yd; break;
            } 
            }
        }
        int nx=xb,ny=yb,xd=x,yd=y;
         while(nx!=xd || ny!=yd){
             if(ny>yd){
                if(conteneurs[nx][ny-1].getText().equals("")) ny--;
                else nx++;
                positionerBras(nx,ny);
            }
            else if(ny<yd){
                if(conteneurs[nx][ny+1].getText().equals("")) ny++;
                else nx++;
                    positionerBras(nx,ny); 
            }
            else if(nx>xd ){ nx--;
                positionerBras(nx,ny);
            }
            else if(nx<xd){ nx++;
                positionerBras(nx,ny);
            }
            
        }   
    }
     private void positionerBras(int xd, int yd){
         
         while(!(conteneurs[xd][yd].getX()>=bras.getX() && bras.getX()+bras.getWidth()>=conteneurs[xd][yd].getX()+conteneurs[xd][yd].getWidth() ||
            conteneurs[xd][yd].getY()<=bras.getY() && bras.getY()+bras.getHeight()>=conteneurs[xd][yd].getY()+conteneurs[xd][yd].getHeight())){
            int x=bras.getX(), y = bras.getY();
            if(conteneurs[xd][yd].getX()<x){
                
                x--;
            }
            else if(conteneurs[xd][yd].getX()>x){
                x++;
            }
            if(conteneurs[xd][yd].getY()<y){
                y--;
            }
            else if(conteneurs[xd][yd].getY()>y){
                y++;
            }
            bras.setBounds(x,y,82,52);
            support.setBounds(bras.getX()+25,support.getY(),support.getWidth(),support.getHeight());
            fil.setBounds(support.getX()+11,support.getY()+support.getHeight(),5,bras.getY()-support.getHeight());
            try{
                cont.sleep(vitesse-(vitesse/2));
            }catch(Exception e){
                e.printStackTrace();
            } 
            
        }   
    }  
   private int distance(int xd, int yd, int xa, int ya){
       return (Math.abs(conteneurs[xd][yd].getX()-conteneurs[xa][ya].getX())+Math.abs(conteneurs[xd][yd].getY()-conteneurs[xa][ya].getY()));
   }
   private void deplacementComplet(int x1, int y1, int x2, int y2){
        prog1.setMaximum(distance(x1, y1, 2, y1)+distance(x2, y2, 2, y2)+distance(x2, y2, x1, y1));
        prog1.setValue(0);
        int xPos = conteneurs[x1][y1].getX(), yPos = conteneurs[x1][y1].getY(); 
        int newXDep=x1, newYDep=y1;
        JFormattedTextField conteneurTemp;
        int xb=x1, yb=y1;
                 //Le bras s'en va à la recherche du conteneur 
            for(int p=0; p<3; p++){
                for(int q=0; q<8; q++){
                    if(conteneurs[p][q].getX()>=bras.getX() && bras.getX()+bras.getWidth()>=conteneurs[p][q].getX()+conteneurs[p][q].getWidth() ||
                        conteneurs[p][q].getY()<=bras.getY() && bras.getY()+bras.getHeight()>=conteneurs[p][q].getY()+conteneurs[p][q].getHeight()){
                       xb=p; yb=q; 
                } 
                }
            }  
            //Le bras a chargé le conteneur, maintenant il passe au déplacement
        positionerBras(x1, y1);
        while(newXDep!=x2 || newYDep!=y2){
            
            if(newYDep==y2){
                newXDep--; deplacement(x1, y1, newXDep,  newYDep);
            }
            else if(newXDep<x2 && newYDep<y2){
                if(conteneurs[newXDep][newYDep+1].getText().equals(""))  newYDep++;
                else newXDep++;
                deplacement(x1,  y1, newXDep,  newYDep);  
            }
            else if(newXDep<x2 && newYDep>y2){
                if(conteneurs[newXDep][newYDep-1].getText().equals(""))  newYDep--;
                else newXDep++;
                deplacement(x1,  y1, newXDep,  newYDep);
            }
            else if(newXDep>x2 && newYDep==y2){
                newXDep--; deplacement(x1,  y1,newXDep,  newYDep);
            }
            else if(newXDep>x2 && newYDep<y2){
                if(conteneurs[newXDep][newYDep+1].getText().equals(""))  newYDep++;
                else newXDep++;
                deplacement(x1,  y1, newXDep,  newYDep);
            }
            else if(newXDep>x2 && newYDep>y2){
                if(conteneurs[newXDep][newYDep-1].getText().equals(""))  newYDep--;
                else newXDep++;
                deplacement(x1,  y1, newXDep,  newYDep);  
            }
            else if(newXDep==x2 && newYDep<y2){
                if(conteneurs[newXDep][newYDep+1].getText().equals(""))
                    newYDep++;
                else newXDep++;
                deplacement(x1,  y1, newXDep,  newYDep);
            }
            else if(newXDep==x2 && newYDep>y2){
                if(conteneurs[newXDep][newYDep-1].getText().equals(""))
                    newYDep--;
                else newXDep++;
                deplacement(x1,  y1, newXDep,  newYDep);
            }
            //generation de la transition effectué
            String s= ""+conteneurs[x1][y1].getText()+"   ("+x1+","+y1+")  ("+newXDep+","+newYDep+")   ("+x2+","+y2+")";
            transitions.addElement(s);
            System.out.println(s);
        }
        //Post condition de deplacement
        System.out.println();System.out.println();System.out.println();
        conteneurTemp=conteneurs[x1][y1];
        conteneurs[x1][y1]=conteneurs[x2][y2];     
        conteneurs[x2][y2]=conteneurTemp;
        conteneurs[x2][y2].setBackground(Color.WHITE);
        conteneurs[x1][y1].setBounds(xPos, yPos,conteneurs[x2][y2].getWidth(),conteneurs[x2][y2].getHeight());
         
   }
}
