/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_algo_opti;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import static java.awt.Font.BOLD;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import javafx.scene.layout.Border;
import static javafx.scene.text.Font.font;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author GILLES KEMGOUM
 */
public class Simulation extends javax.swing.JFrame {

    /**
     * Creates new form Projet_Algo
     */
    public Simulation(JLabel[][] tjl, JFormattedTextField[][] tjtf ) {
        
        initComponents();
        
        //Récupération des conteneurs de la plateforme initiale
        conteneurs[0][0]=j00;
        conteneurs[0][1]=j01;
        conteneurs[0][2]=j02;
        conteneurs[0][3]=j03;
        conteneurs[0][4]=j04;
        conteneurs[0][5]=j05;
        conteneurs[0][6]=j06;
        conteneurs[0][7]=j07;
        
        conteneurs[1][0]=j10;
        conteneurs[1][0]=j10;
        conteneurs[1][1]=j11;
        conteneurs[1][2]=j12;
        conteneurs[1][3]=j13;
        conteneurs[1][4]=j14;
        conteneurs[1][5]=j15;
        conteneurs[1][6]=j16;
        conteneurs[1][7]=j17;
         
        conteneurs[2][0]=j20;
        conteneurs[2][1]=j21;
        conteneurs[2][2]=j22;
        conteneurs[2][3]=j23;
        conteneurs[2][4]=j24;
        conteneurs[2][5]=j25;
        conteneurs[2][6]=j26;
        conteneurs[2][7]=j27;
        
        //recuperation des valeurs initiales
        initiales[0][0]=l00;
        initiales[0][1]=l01;
        initiales[0][2]=l02;
        initiales[0][3]=l03;
        initiales[0][4]=l04;
        initiales[0][5]=l05;
        initiales[0][6]=l06;
        initiales[0][7]=l07;
        
        initiales[1][0]=l10;
        initiales[1][0]=l10;
        initiales[1][1]=l11;
        initiales[1][2]=l12;
        initiales[1][3]=l13;
        initiales[1][4]=l14;
        initiales[1][5]=l15;
        initiales[1][6]=l16;
        initiales[1][7]=l17;
         
        initiales[2][0]=l20;
        initiales[2][1]=l21;
        initiales[2][2]=l22;
        initiales[2][3]=l23;
        initiales[2][4]=l24;
        initiales[2][5]=l25;
        initiales[2][6]=l26;
        initiales[2][7]=l27;
         
        j27.getParent().add(bras);
        
        
        bras.setEditable(false);
        bras.setBackground(Color.ORANGE);
        bras.setBorder(new EmptyBorder(2,2,2,2));
        bras.setBounds(brasVisible.getX(),brasVisible.getY(),brasVisible.getWidth(),brasVisible.getHeight());
        brasVisible.getParent().remove(brasVisible);        
        
        //Initialisation des composants
        
        
         for(int i=0; i<3; i++){
            for(int j=0; j<8; j++){ 
            conteneurs[i][j].setBounds(conteneurs[0][j].getX(),conteneurs[i][0].getY(),conteneurs[0][0].getWidth(),conteneurs[0][0].getHeight());
            System.out.print("("+i+":"+j+")"+conteneurs[i][j].getX()+","+conteneurs[i][j].getY()+"   ");
            } 
            System.out.println();
         }    
        for(int i=0; i<3; i++){
            for(int j=0; j<8; j++){ 
               
               initiales[i][j].setText(tjl[i][j].getText());
               finales[i][j]= new JFormattedTextField(tjtf[i][j].getText());
               
               finales[i][j].setBounds(conteneurs[i][j].getX(),conteneurs[i][j].getY(),conteneurs[i][j].getWidth(),conteneurs[i][j].getHeight());
               
               if(!tjl[i][j].getText().equals("")){
                   conteneurs[i][j].setText(tjl[i][j].getText());
                   conteneurs[i][j].setFont(new Font(Font.SERIF, Font.ITALIC, 25)); 
                   
                   conteneurs[i][j].setEnabled(true);
                   conteneurs[i][j].setBackground(Color.GREEN);
                   conteneurs[i][j].setEditable(false);
                   
               } 
               
               // System.out.println(conteneurs[i][j].getText()+"("+conteneurs[i][j].getX()+":"+conteneurs[i][j].getY()+")  "+finales[i][j].getText()
              //+"("+finales[i][j].getX()+":"+finales[i][j].getY()+")");
            }
        } 
        
    }
    private void arranger(){
        for(int i=0; i<3; i++){
            for(int j=0; j<8; j++){ 
                if(conteneurs[i][j].getText().equals("")){
                    conteneurs[i][j].getParent().remove(conteneurs[i][j]);     
                } 
                else conteneurs[i][j].setBounds(conteneurs[i][j].getX(),conteneurs[i][j].getY(),80,50);
            }
        }
        
    }



    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Valeur = new javax.swing.JLabel();
        pan1 = new javax.swing.JPanel();
        pan2 = new javax.swing.JPanel();
        pan3 = new javax.swing.JPanel();
        pan4 = new javax.swing.JPanel();
        j10 = new javax.swing.JFormattedTextField();
        j11 = new javax.swing.JFormattedTextField();
        j12 = new javax.swing.JFormattedTextField();
        j13 = new javax.swing.JFormattedTextField();
        j03 = new javax.swing.JFormattedTextField();
        j02 = new javax.swing.JFormattedTextField();
        j01 = new javax.swing.JFormattedTextField();
        j00 = new javax.swing.JFormattedTextField();
        j20 = new javax.swing.JFormattedTextField();
        j21 = new javax.swing.JFormattedTextField();
        j22 = new javax.swing.JFormattedTextField();
        j23 = new javax.swing.JFormattedTextField();
        j04 = new javax.swing.JFormattedTextField();
        j14 = new javax.swing.JFormattedTextField();
        j24 = new javax.swing.JFormattedTextField();
        j05 = new javax.swing.JFormattedTextField();
        j15 = new javax.swing.JFormattedTextField();
        j25 = new javax.swing.JFormattedTextField();
        j06 = new javax.swing.JFormattedTextField();
        j16 = new javax.swing.JFormattedTextField();
        j26 = new javax.swing.JFormattedTextField();
        j07 = new javax.swing.JFormattedTextField();
        j17 = new javax.swing.JFormattedTextField();
        j27 = new javax.swing.JFormattedTextField();
        support = new java.awt.Button();
        brasVisible = new javax.swing.JFormattedTextField();
        fil = new java.awt.Button();
        jLabel11 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        l20 = new javax.swing.JLabel();
        l10 = new javax.swing.JLabel();
        l00 = new javax.swing.JLabel();
        l21 = new javax.swing.JLabel();
        l11 = new javax.swing.JLabel();
        l01 = new javax.swing.JLabel();
        l22 = new javax.swing.JLabel();
        l12 = new javax.swing.JLabel();
        l02 = new javax.swing.JLabel();
        l23 = new javax.swing.JLabel();
        l13 = new javax.swing.JLabel();
        l03 = new javax.swing.JLabel();
        l24 = new javax.swing.JLabel();
        l14 = new javax.swing.JLabel();
        l04 = new javax.swing.JLabel();
        l25 = new javax.swing.JLabel();
        l15 = new javax.swing.JLabel();
        l05 = new javax.swing.JLabel();
        l26 = new javax.swing.JLabel();
        l27 = new javax.swing.JLabel();
        l16 = new javax.swing.JLabel();
        l06 = new javax.swing.JLabel();
        l17 = new javax.swing.JLabel();
        l07 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        Suivant = new java.awt.Button();
        jPanel6 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();

        Valeur.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        Valeur.setForeground(new java.awt.Color(0, 102, 102));
        Valeur.setText("Ok");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocation(new java.awt.Point(300, 40));
        setResizable(false);

        pan1.setBackground(new java.awt.Color(255, 255, 204));

        pan2.setBackground(new java.awt.Color(204, 255, 204));

        pan3.setBackground(new java.awt.Color(255, 204, 204));
        pan3.setAutoscrolls(true);
        pan3.setMinimumSize(new java.awt.Dimension(32767, 32767));

        pan4.setBackground(new java.awt.Color(153, 204, 255));
        pan4.setMinimumSize(new java.awt.Dimension(32767, 32767));

        j10.setEnabled(false);
        j10.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j10FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j10FocusLost(evt);
            }
        });

        j11.setEnabled(false);
        j11.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j11FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j11FocusLost(evt);
            }
        });
        j11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                j11ActionPerformed(evt);
            }
        });

        j12.setEnabled(false);
        j12.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j12FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j12FocusLost(evt);
            }
        });

        j13.setEnabled(false);
        j13.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j13FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j13FocusLost(evt);
            }
        });

        j03.setEnabled(false);
        j03.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                j03FocusLost(evt);
            }
        });
        j03.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                j03ActionPerformed(evt);
            }
        });

        j02.setEnabled(false);
        j02.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                j02FocusLost(evt);
            }
        });

        j01.setEnabled(false);
        j01.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                j01FocusLost(evt);
            }
        });

        j00.setEnabled(false);
        j00.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j00FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j00FocusLost(evt);
            }
        });

        j20.setEnabled(false);
        j20.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j20FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j20FocusLost(evt);
            }
        });

        j21.setEnabled(false);
        j21.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j21FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j21FocusLost(evt);
            }
        });

        j22.setEnabled(false);
        j22.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j22FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j22FocusLost(evt);
            }
        });

        j23.setEnabled(false);
        j23.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j23FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j23FocusLost(evt);
            }
        });

        j04.setEnabled(false);
        j04.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j04FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j04FocusLost(evt);
            }
        });

        j14.setEnabled(false);
        j14.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j14FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j14FocusLost(evt);
            }
        });

        j24.setEnabled(false);
        j24.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j24FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j24FocusLost(evt);
            }
        });

        j05.setEnabled(false);
        j05.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                j05FocusLost(evt);
            }
        });

        j15.setEnabled(false);
        j15.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j15FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j15FocusLost(evt);
            }
        });
        j15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                j15ActionPerformed(evt);
            }
        });

        j25.setEnabled(false);
        j25.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j25FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j25FocusLost(evt);
            }
        });

        j06.setEnabled(false);
        j06.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                j06FocusLost(evt);
            }
        });

        j16.setEnabled(false);
        j16.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j16FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j16FocusLost(evt);
            }
        });

        j26.setEnabled(false);
        j26.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j26FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j26FocusLost(evt);
            }
        });

        j07.setEnabled(false);
        j07.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                j07FocusLost(evt);
            }
        });
        j07.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                j07ActionPerformed(evt);
            }
        });

        j17.setEnabled(false);
        j17.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j17FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j17FocusLost(evt);
            }
        });

        j27.setEnabled(false);
        j27.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                j27FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                j27FocusLost(evt);
            }
        });

        support.setBackground(new java.awt.Color(0, 0, 102));
        support.setEnabled(false);
        support.setLabel("");

        brasVisible.setEditable(false);
        brasVisible.setOpaque(false);
        brasVisible.setRequestFocusEnabled(false);

        fil.setBackground(new java.awt.Color(255, 204, 204));
        fil.setEnabled(false);
        fil.setLabel("");

        javax.swing.GroupLayout pan4Layout = new javax.swing.GroupLayout(pan4);
        pan4.setLayout(pan4Layout);
        pan4Layout.setHorizontalGroup(
            pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan4Layout.createSequentialGroup()
                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pan4Layout.createSequentialGroup()
                        .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pan4Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan4Layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(j00, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(j01, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(j02, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(j03, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(j04, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan4Layout.createSequentialGroup()
                                        .addComponent(j10, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(j11, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(j12, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(j13, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(j14, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(pan4Layout.createSequentialGroup()
                                        .addComponent(j20, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(j21, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(j22, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(j23, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(j24, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))))
                            .addGroup(pan4Layout.createSequentialGroup()
                                .addGap(332, 332, 332)
                                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pan4Layout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addComponent(fil, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(support, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(6, 6, 6)
                        .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pan4Layout.createSequentialGroup()
                                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(j05, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                                    .addComponent(j15))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(j16)
                                    .addComponent(j06)))
                            .addGroup(pan4Layout.createSequentialGroup()
                                .addComponent(j25, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(j26, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(j07, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(j17, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(j27, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pan4Layout.createSequentialGroup()
                        .addGap(306, 306, 306)
                        .addComponent(brasVisible, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pan4Layout.setVerticalGroup(
            pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan4Layout.createSequentialGroup()
                .addComponent(support, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(fil, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(brasVisible, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(j21, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(j23, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(j24, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(j25, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(j26, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(j27, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(j20)
                    .addComponent(j22, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(j10)
                    .addComponent(j11)
                    .addComponent(j13)
                    .addComponent(j12)
                    .addComponent(j14)
                    .addComponent(j17, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(j15)
                    .addComponent(j16))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pan4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                        .addComponent(j07, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(j04, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(j05, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(j00)
                    .addComponent(j06)
                    .addComponent(j01)
                    .addComponent(j02, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(j03, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );

        javax.swing.GroupLayout pan3Layout = new javax.swing.GroupLayout(pan3);
        pan3.setLayout(pan3Layout);
        pan3Layout.setHorizontalGroup(
            pan3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pan3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 688, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pan4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pan3Layout.setVerticalGroup(
            pan3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pan4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel11))
        );

        javax.swing.GroupLayout pan2Layout = new javax.swing.GroupLayout(pan2);
        pan2.setLayout(pan2Layout);
        pan2Layout.setHorizontalGroup(
            pan2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan2Layout.createSequentialGroup()
                .addContainerGap(26, Short.MAX_VALUE)
                .addComponent(pan3, javax.swing.GroupLayout.PREFERRED_SIZE, 712, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24))
        );
        pan2Layout.setVerticalGroup(
            pan2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pan3, javax.swing.GroupLayout.PREFERRED_SIZE, 294, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel2.setBackground(new java.awt.Color(204, 204, 255));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 102, 102));
        jLabel2.setText("Conteneurs à ranger:");

        l20.setText("jLabel4");

        l10.setText("jLabel5");

        l00.setText("jLabel6");

        l21.setText("jLabel7");

        l11.setText("jLabel8");

        l01.setText("jLabel9");

        l22.setText("jLabel10");

        l12.setText("jLabel11");

        l02.setText("jLabel12");

        l23.setText("jLabel13");

        l13.setText("jLabel14");

        l03.setText("jLabel15");

        l24.setText("jLabel16");

        l14.setText("jLabel17");

        l04.setText("jLabel18");

        l25.setText("jLabel19");

        l15.setText("jLabel20");

        l05.setText("jLabel21");

        l26.setText("jLabel22");

        l27.setText("jLabel23");

        l16.setText("jLabel24");

        l06.setText("jLabel25");

        l17.setText("jLabel26");

        l07.setText("jLabel27");

        jLabel4.setText("----------------------------------------------------------------------------------------------------------------------------------------------");

        jLabel5.setText("----------------------------------------------------------------------------------------------------------------------------------------------");

        jLabel6.setText("|");

        jLabel7.setText("|");

        jLabel8.setText("|");

        jLabel9.setText("|");

        jLabel10.setText("|");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGap(155, 155, 155)
                            .addComponent(jLabel6))
                        .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING))
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(l20)
                            .addComponent(l10)
                            .addComponent(l00))
                        .addGap(32, 32, 32)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(l21)
                                .addGap(31, 31, 31)
                                .addComponent(l22)
                                .addGap(32, 32, 32)
                                .addComponent(l23))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(l11)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                                        .addComponent(l12))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(l01)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(l02)))
                                .addGap(32, 32, 32)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(l03)
                                    .addComponent(l13))))
                        .addGap(46, 46, 46)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(l24)
                                .addGap(33, 33, 33)
                                .addComponent(l25))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(l04)
                                    .addComponent(l14))
                                .addGap(33, 33, 33)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(l15)
                                    .addComponent(l05))))
                        .addGap(34, 34, 34)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(l26)
                                .addGap(34, 34, 34)
                                .addComponent(l27))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(l16)
                                    .addComponent(l06))
                                .addGap(34, 34, 34)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(l17, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(l07, javax.swing.GroupLayout.Alignment.TRAILING))))
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel8)
                            .addComponent(jLabel10))
                        .addContainerGap())
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(l20)
                    .addComponent(l21)
                    .addComponent(l22)
                    .addComponent(l23)
                    .addComponent(l24)
                    .addComponent(l25)
                    .addComponent(l26)
                    .addComponent(l27)
                    .addComponent(jLabel8))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(l05)
                            .addComponent(l04)
                            .addComponent(l06)
                            .addComponent(l07))
                        .addContainerGap())
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(l10)
                            .addComponent(l11)
                            .addComponent(l12)
                            .addComponent(l13)
                            .addComponent(l14)
                            .addComponent(l15)
                            .addComponent(l16)
                            .addComponent(l17))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(l00)
                                    .addComponent(l01)
                                    .addComponent(l02)
                                    .addComponent(l03)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel7)))))
        );

        Suivant.setBackground(new java.awt.Color(204, 255, 204));
        Suivant.setLabel("Simuler");
        Suivant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SuivantActionPerformed(evt);
            }
        });

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Arial Narrow", 1, 48)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 204, 204));
        jLabel1.setText("   Deplacements");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 337, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(182, 182, 182))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout pan1Layout = new javax.swing.GroupLayout(pan1);
        pan1.setLayout(pan1Layout);
        pan1Layout.setHorizontalGroup(
            pan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pan1Layout.createSequentialGroup()
                            .addGap(197, 197, 197)
                            .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 348, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(63, 63, 63)
                            .addComponent(Suivant, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(pan2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pan1Layout.setVerticalGroup(
            pan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan1Layout.createSequentialGroup()
                .addGroup(pan1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(Suivant, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(pan2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(21, 21, 21))
        );

        jPanel12.setBackground(new java.awt.Color(0, 102, 102));

        jLabel3.setFont(new java.awt.Font("Rockwell Condensed", 1, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("TP d'algorithmique 2020/2021");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(243, 243, 243)
                .addComponent(jLabel3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel12, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pan1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pan1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private static void statut(){
        for(int i=0; i<3; i++){
            for(int j=0; j<8; j++){
                initiales[i][j].setForeground(new java.awt.Color(0, 0, 0));
                for(int k=0; k<3; k++){
                     for(int l=0; l<8; l++){
                        if(initiales[i][j].getText().equals(conteneurs[k][l].getText())){
                           initiales[i][j].setForeground(new java.awt.Color(255, 255, 255));
                        }
                     } 
                }     
            } 
        }
    }
    private void ranger(JFormattedTextField dep, int xdep, int ydep){
        
    }
    private void j11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_j11ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_j11ActionPerformed

    private void j03ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_j03ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_j03ActionPerformed
  
    private void j00FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j00FocusLost
        
    }//GEN-LAST:event_j00FocusLost

    private void j00FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j00FocusGained
         
        
    }//GEN-LAST:event_j00FocusGained

    private void j01FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j01FocusLost
        
    }//GEN-LAST:event_j01FocusLost

    private void j11FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j11FocusLost
        
    }//GEN-LAST:event_j11FocusLost

    private void j21FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j21FocusGained
     
    }//GEN-LAST:event_j21FocusGained

    private void j10FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j10FocusLost
       
    }//GEN-LAST:event_j10FocusLost

    private void j12FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j12FocusLost

    }//GEN-LAST:event_j12FocusLost

    private void j22FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j22FocusLost
        
    }//GEN-LAST:event_j22FocusLost

    private void j13FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j13FocusLost
        
    }//GEN-LAST:event_j13FocusLost

    private void j03FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j03FocusLost
        
    }//GEN-LAST:event_j03FocusLost

    private void j02FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j02FocusLost
       
    }//GEN-LAST:event_j02FocusLost

    private void j20FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j20FocusLost
        
    }//GEN-LAST:event_j20FocusLost

    private void j21FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j21FocusLost
        
    }//GEN-LAST:event_j21FocusLost

    private void j23FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j23FocusLost
      
    }//GEN-LAST:event_j23FocusLost

    private void j14FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j14FocusLost
       
    }//GEN-LAST:event_j14FocusLost

    private void j15FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j15FocusLost
        
    }//GEN-LAST:event_j15FocusLost

    private void j15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_j15ActionPerformed
        
    }//GEN-LAST:event_j15ActionPerformed

    private void j16FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j16FocusLost
        
    }//GEN-LAST:event_j16FocusLost

    private void j17FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j17FocusLost
       
    }//GEN-LAST:event_j17FocusLost

    private void j07FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j07FocusLost
       
    }//GEN-LAST:event_j07FocusLost

    private void j07ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_j07ActionPerformed
         
    }//GEN-LAST:event_j07ActionPerformed

    private void j06FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j06FocusLost
       
    }//GEN-LAST:event_j06FocusLost

    private void j05FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j05FocusLost
       
    }//GEN-LAST:event_j05FocusLost

    private void j04FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j04FocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_j04FocusGained

    private void j04FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j04FocusLost
       
    }//GEN-LAST:event_j04FocusLost

    private void j24FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j24FocusLost
       
    }//GEN-LAST:event_j24FocusLost

    private void j25FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j25FocusGained
        
    }//GEN-LAST:event_j25FocusGained

    private void j25FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j25FocusLost
       
    }//GEN-LAST:event_j25FocusLost

    private void j26FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j26FocusLost
       
    }//GEN-LAST:event_j26FocusLost

    private void j27FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j27FocusLost
       
    }//GEN-LAST:event_j27FocusLost

    private void SuivantActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SuivantActionPerformed
         cont = new Thread(new Simulation.deplacer());
         cont.start();
    }//GEN-LAST:event_SuivantActionPerformed

    private void j10FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j10FocusGained
       
    }//GEN-LAST:event_j10FocusGained

    private void j11FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j11FocusGained
        
    }//GEN-LAST:event_j11FocusGained

    private void j12FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j12FocusGained
        
    }//GEN-LAST:event_j12FocusGained

    private void j13FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j13FocusGained
       
    }//GEN-LAST:event_j13FocusGained

    private void j20FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j20FocusGained
         
    }//GEN-LAST:event_j20FocusGained

    private void j22FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j22FocusGained
        
    }//GEN-LAST:event_j22FocusGained

    private void j23FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j23FocusGained
         
    }//GEN-LAST:event_j23FocusGained

    private void j14FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j14FocusGained
        
    }//GEN-LAST:event_j14FocusGained

    private void j15FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j15FocusGained
        
    }//GEN-LAST:event_j15FocusGained

    private void j16FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j16FocusGained
        
    }//GEN-LAST:event_j16FocusGained

    private void j17FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j17FocusGained
         
    }//GEN-LAST:event_j17FocusGained

    private void j27FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j27FocusGained
        
    }//GEN-LAST:event_j27FocusGained

    private void j26FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j26FocusGained
        
    }//GEN-LAST:event_j26FocusGained

    private void j24FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_j24FocusGained
         
    }//GEN-LAST:event_j24FocusGained

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Simulation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Simulation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Simulation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Simulation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JFormattedTextField[][] cont = new JFormattedTextField[3][8];
                JLabel[][] jl = new JLabel[3][8];
                for(int i=0; i<3; i++){
                    for(int j=0; j<8; j++){
                       cont[i][j] = new JFormattedTextField("");
                       jl[i][j] = new JLabel("");  
                    }   
                }
                jl[0][1].setText("A");
                jl[1][7].setText("B");
                jl[0][7].setText("C"); 
                jl[0][6].setText("D");
                
                cont[2][7].setText("A");
                cont[1][7].setText("B");
                cont[2][6].setText("C");
                cont[0][1].setText("D");
                                
                
                
                
                
                
                new Simulation(jl, cont).setVisible(true);
                
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.awt.Button Suivant;
    public static javax.swing.JLabel Valeur;
    private javax.swing.JFormattedTextField brasVisible;
    private java.awt.Button fil;
    public javax.swing.JFormattedTextField j00;
    private javax.swing.JFormattedTextField j01;
    private javax.swing.JFormattedTextField j02;
    private javax.swing.JFormattedTextField j03;
    private javax.swing.JFormattedTextField j04;
    private javax.swing.JFormattedTextField j05;
    private javax.swing.JFormattedTextField j06;
    private javax.swing.JFormattedTextField j07;
    private javax.swing.JFormattedTextField j10;
    private javax.swing.JFormattedTextField j11;
    private javax.swing.JFormattedTextField j12;
    private javax.swing.JFormattedTextField j13;
    private javax.swing.JFormattedTextField j14;
    private javax.swing.JFormattedTextField j15;
    private javax.swing.JFormattedTextField j16;
    private javax.swing.JFormattedTextField j17;
    private javax.swing.JFormattedTextField j20;
    private javax.swing.JFormattedTextField j21;
    private javax.swing.JFormattedTextField j22;
    private javax.swing.JFormattedTextField j23;
    private javax.swing.JFormattedTextField j24;
    private javax.swing.JFormattedTextField j25;
    private javax.swing.JFormattedTextField j26;
    private javax.swing.JFormattedTextField j27;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JLabel l00;
    private javax.swing.JLabel l01;
    private javax.swing.JLabel l02;
    private javax.swing.JLabel l03;
    private javax.swing.JLabel l04;
    private javax.swing.JLabel l05;
    private javax.swing.JLabel l06;
    private javax.swing.JLabel l07;
    private javax.swing.JLabel l10;
    private javax.swing.JLabel l11;
    private javax.swing.JLabel l12;
    private javax.swing.JLabel l13;
    private javax.swing.JLabel l14;
    private javax.swing.JLabel l15;
    private javax.swing.JLabel l16;
    private javax.swing.JLabel l17;
    private javax.swing.JLabel l20;
    private javax.swing.JLabel l21;
    private javax.swing.JLabel l22;
    private javax.swing.JLabel l23;
    private javax.swing.JLabel l24;
    private javax.swing.JLabel l25;
    private javax.swing.JLabel l26;
    private javax.swing.JLabel l27;
    private javax.swing.JPanel pan1;
    private javax.swing.JPanel pan2;
    private javax.swing.JPanel pan3;
    private javax.swing.JPanel pan4;
    private java.awt.Button support;
    // End of variables declaration//GEN-END:variables
    private static javax.swing.JFormattedTextField[][] conteneurs= new javax.swing.JFormattedTextField[4][8];
    private static javax.swing.JFormattedTextField[][] finales= new javax.swing.JFormattedTextField[3][8];
    private static javax.swing.JLabel[][] initiales= new javax.swing.JLabel[3][8];
    private static javax.swing.JLabel[][] init= new javax.swing.JLabel[3][8];
    private Thread cont;
    private int Xpositions[][] = new int[3][8];
    private int Ypositions[][] = new int[3][8];
    Graphics2D br_su;
    private static javax.swing.JFormattedTextField bras = new javax.swing.JFormattedTextField();
    
    class deplacer implements Runnable{
        
        deplacer(){
            arranger();
        }
        public void run(){
             
            for(int i=0; i<3; i++){
                for(int j=0; j<8; j++){
                    //conservation des couleurs initiales
                    for(int l=i; l<3; l++){
                            int k=j;
                            if(l!=i)k=0;
                            for(int m=k; m<8; m++){
                               conteneurs[i][j].setBackground(Color.GREEN);
                            }
                        }
                   if(!finales[i][j].getText().equals("")){
                        int xd=0, yd=0;
                        for(int l=0; l<3; l++){
                            for(int m=0; m<8; m++){
                               if(conteneurs[l][m].getText().equals(finales[i][j].getText())){
                                   xd=l;
                                   yd=m;
                               }
                            }
                        }
                        
                        conteneurs[xd][yd].setBackground(Color.YELLOW);
                        deplacementComplet(xd, yd,i,j);
                        conteneurs[xd][yd].setBackground(Color.WHITE);
                        bras.setBackground(Color.orange);
                    }
                }            
            }
            initialiserBras();
        }
    }
    private void initialiserBras(){
        while(bras.getY()>brasVisible.getY()){
            bras.setBounds(bras.getX(),bras.getY()-1,brasVisible.getWidth(),brasVisible.getHeight());
            support.setBounds(bras.getX()+25,support.getY(),support.getWidth(),support.getHeight());
            fil.setBounds(support.getX()+11,support.getY()+support.getHeight(),5,bras.getY()-support.getHeight());
            try{
                cont.sleep(3);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        bras.setBackground(Color.PINK);
    }
    private void deplacement(int xd, int yd, int i, int j){
        //Le bras s'en va à la recherche du conteneur 
        positionerBras(xd, yd);
        //Le bras a chargé le conteneur, maintenant il passe au déplacement
        bras.setBackground(Color.CYAN); 
        Xpositions[xd][yd]=conteneurs[xd][yd].getX();
        Ypositions[xd][yd]=conteneurs[xd][yd].getY();
        Xpositions[i][j]=conteneurs[i][j].getX();
        Ypositions[i][j]=conteneurs[i][j].getY();
        while(conteneurs[xd][yd].getX()!=conteneurs[i][j].getX() || conteneurs[xd][yd].getY()!=conteneurs[i][j].getY()){
            if(Xpositions[i][j]<Xpositions[xd][yd] && Ypositions[i][j]<Ypositions[xd][yd]){
                Xpositions[xd][yd]--; Ypositions[xd][yd]--;
            }
            else if(Xpositions[i][j]<Xpositions[xd][yd] && Ypositions[i][j]>Ypositions[xd][yd]){
                Xpositions[xd][yd]--; Ypositions[xd][yd]++;
            }
            else if(Xpositions[i][j]<Xpositions[xd][yd] && Ypositions[i][j]==Ypositions[xd][yd]){
                Xpositions[xd][yd]--;
            }
            else if(Xpositions[i][j]>Xpositions[xd][yd] && Ypositions[i][j]<Ypositions[xd][yd]){
                Xpositions[xd][yd]++; Ypositions[xd][yd]--;
            }
            else if(Xpositions[i][j]>Xpositions[xd][yd] && Ypositions[i][j]>Ypositions[xd][yd]){
                Xpositions[xd][yd]++; Ypositions[xd][yd]++;
            }
            else if(Xpositions[i][j]>Xpositions[xd][yd] && Ypositions[i][j]==Ypositions[xd][yd]){
                Xpositions[xd][yd]++;
            }
            else if(Xpositions[i][j]==Xpositions[xd][yd] && Ypositions[i][j]<Ypositions[xd][yd]){
                Ypositions[xd][yd]--;
            }
            else if(Xpositions[i][j]==Xpositions[xd][yd] && Ypositions[i][j]>Ypositions[xd][yd]){
                Ypositions[xd][yd]++;
            }
            bras.setBounds(Xpositions[xd][yd],Ypositions[xd][yd],brasVisible.getWidth(),brasVisible.getHeight());
            support.setBounds(bras.getX()+25,support.getY(),support.getWidth(),support.getHeight());
            fil.setBounds(support.getX()+11,support.getY()+support.getHeight(),5,bras.getY()-support.getHeight());
            conteneurs[xd][yd].setBounds(Xpositions[xd][yd],Ypositions[xd][yd],finales[i][j].getWidth(),finales[i][j].getHeight());
            try{
                cont.sleep(5);
            }catch(Exception e){
                e.printStackTrace();
            }   
        }
    }
    
     private void positionerBras(int xd, int yd){
         while(!(conteneurs[xd][yd].getX()>=bras.getX() && bras.getX()+bras.getWidth()>=conteneurs[xd][yd].getX()+conteneurs[xd][yd].getWidth() &&
            conteneurs[xd][yd].getY()<=bras.getY() && bras.getY()+bras.getHeight()>=conteneurs[xd][yd].getY()+conteneurs[xd][yd].getHeight())){
            int x=bras.getX(), y = bras.getY();
            if(conteneurs[xd][yd].getX()<x) x--;
            else if(conteneurs[xd][yd].getX()>x)x++;
            if(conteneurs[xd][yd].getY()<y) y--;
            else if(conteneurs[xd][yd].getY()>y) y++;
            bras.setBounds(x,y,82,52);
            support.setBounds(bras.getX()+25,support.getY(),support.getWidth(),support.getHeight());
            fil.setBounds(support.getX()+11,support.getY()+support.getHeight(),5,bras.getY()-support.getHeight());
            try{
                cont.sleep(3);
            }catch(Exception e){
                e.printStackTrace();
            } 

        }   
    }  

   private void deplacementComplet(int x1, int y1, int x2, int y2){
        int xPos = conteneurs[x1][y1].getX(), yPos = conteneurs[x1][y1].getY(); 
        int newXDep=x1, newYDep=y1;
        JFormattedTextField conteneurTemp;
        while(newXDep!=x2 || newYDep!=y2){
            if(newXDep<x2 && newYDep==y2){
                newXDep++; deplacement(x1, y1, newXDep,  newYDep);
            }
            else if(newXDep<x2 && newYDep<y2){
                newXDep++;deplacement(x1,  y1, newXDep,  newYDep);
                newYDep++;deplacement(x1,  y1, newXDep,  newYDep);  
            }
            else if(newXDep<x2 && newYDep>y2){
                newYDep--; deplacement(x1,  y1, newXDep,  newYDep);
                newXDep++;deplacement(x1,  y1, newXDep,  newYDep);
            }
            else if(newXDep>x2 && newYDep==y2){
                newXDep--;deplacement(x1,  y1,newXDep,  newYDep);
            }
            else if(newXDep>x2 && newYDep<y2){
                newXDep--;deplacement(x1,  y1, newXDep,  newYDep);
                newYDep++;deplacement(x1,  y1, newXDep,  newYDep);
            }
            else if(newXDep>x2 && newYDep>y2){
                newXDep--; deplacement(x1,  y1, newXDep,  newYDep);
                newYDep--;deplacement(x1,  y1, newXDep,  newYDep);
            }
            else if(newXDep==x2 && newYDep<y2){
                newYDep++; deplacement(x1,  y1, newXDep,  newYDep);
            }
            else if(newXDep==x2 && newYDep>y2){
                newYDep--; deplacement(x1,  y1, newXDep,  newYDep);   
            }
            //generation de la transition effectué
            System.out.println(x1+","+y1+"  "+newXDep+","+newYDep+"   "+x2+","+y2);
        }
        //Post condition de deplacement
        System.out.println();System.out.println();System.out.println();
        conteneurTemp=conteneurs[x1][y1];
        conteneurs[x1][y1]=conteneurs[x2][y2];
        conteneurs[x2][y2]=conteneurTemp;
        //reinitialisation des positions de conteneurs
        conteneurs[x2][y2].setBackground(Color.WHITE);
        conteneurs[x1][y1].setBounds(xPos, yPos,conteneurs[x2][y2].getWidth(),conteneurs[x2][y2].getHeight());
       }
}
